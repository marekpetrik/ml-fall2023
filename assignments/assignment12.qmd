---
title: "Assignment 12"
subtitle: CS 750/850 Machine Learning
output: pdf_document
---

- **Due**: Tuesday 11/21 at 11:59PM
- **Submission**: Turn in as a **PDF** and the **source code** (qmd) on [MyCourses](http://mycourses.unh.edu).


# Problem 1

In words, describe the results that you would expect if you performed K-means clustering of the eight shoppers in Figure 12.16, on the basis of their sock and computer purchases, with $K= 2$. Give three answers, one for each of the variable scalings displayed. Explain.

# Problem 2

Pick a reasonable dataset of your choice and identify clusters using k-means and a Gaussian mixture model. Choose an appropriate value for $k$. Compare the quality of the two clusterings computed. Discuss some options for a good metric. 
