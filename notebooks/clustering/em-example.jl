# note that Distributions already includes a mixture distribution
# but we do not want to use it here because that would defeat the purpose
using Distributions
using Plots
using Accessors
using LaTeXStrings
pgfplotsx()

# models parameters of the GMM
struct GMM
    comps :: Vector{Normal}
    weights :: Categorical
end

# construct the parameters of the distributions
truegmm = GMM([Normal(-2, 1), Normal(2, 5)], Categorical([0.6, 0.4]))
falsegmm = GMM([Normal(-7, 5), Normal(5, 1)], Categorical([0.4, 0.6]))

function generate_data(count :: Integer, θ :: GMM)
    zs = rand(θ.weights, count)
    xs = [rand(θ.comps[z]) for z ∈ zs]
    (xs=xs, zs=zs)
end

data = generate_data(1000, truegmm)
histogram(data.xs)

# the loglikelihood function for a set of parameters given
function loglikelihood(xs :: Vector{Float64}, θ :: GMM)
    ll = 0
    for i ∈ eachindex(xs)
        pc = (pdf(c, xs[i]) * p for (p,c) ∈ zip(θ.weights.p, θ.comps))
        ll += log(sum(pc))
    end
    ll
end

println("Tue loglike: ", loglikelihood(data.xs, truegmm))

### alpha plots

# compute log likelihoods
function lls_α(xs, θ)
    αs = range(0.1,0.99,length=100)
    lls = zeros(length(αs))
    for (i,α) ∈ enumerate(αs)
        newgmm = @set θ.weights = Categorical(α, 1-α)
        lls[i] = loglikelihood(xs, newgmm)
    end
    (αs, lls)
end

plot(lls_α(data.xs, truegmm)...)
plot(lls_α(data.xs, falsegmm)...)

### sigma plots

function lls_σ(xs, θ)
    σs = range(0.22,10,length=1000)
    lls = zeros(length(σs))
    for (i,σ) ∈ enumerate(σs)
        newgmm = @set θ.comps[1] = Normal(θ.comps[1].μ, σ)
        lls[i] = loglikelihood(xs, newgmm)
    end
    # plot the precision
    (1.0 ./ σs.^2, lls)
end

###

plot(lls_σ(data.xs, truegmm)...)
plot(lls_σ(data.xs, falsegmm)...)


### lower bound

# the loglikelihood function for a set of parameters given
function ll_lowerbound(xs :: Vector{Float64}, θ :: GMM, θₖ :: GMM)
    ll = 0
    for i ∈ eachindex(xs)
        pz = [θₖ.weights.p[z] * pdf(θₖ.comps[z], xs[i]) for z ∈ eachindex(θₖ.comps)]
        pz /= sum(pz)
        # skip weights of 0
        pc = [w*(logpdf(c, xs[i]) + log(α) - log(w))
              for (c, α, w) ∈ zip(θ.comps, θ.weights.p, pz) if w > 1e-5]
        @assert !any(isnan.(pc))
        ll += sum(pc)
    end
    ll
end

ll_lowerbound(data.xs, truegmm, truegmm)

### lower bound plots

function lls_lower_σ(xs, θ, θₖ; σmin = 0.3)
    σs = range(σmin,10,length=100)
    lls = zeros(length(σs))
    for (i,σ) ∈ enumerate(σs)
        newgmm = @set θ.comps[1].σ = σ 
        lls[i] = ll_lowerbound(xs, newgmm, θₖ)
    end
    # plot it in terms of the precision
    (1 ./ σs.^2, lls)
end

###

plot(lls_σ(data.xs, truegmm)..., label = "True LogLike", width = 2,
     xlabel = L"Precision = $1 / \sigma^2$", ylabel = "Log Likelihood")
plot!(lls_lower_σ(data.xs, truegmm, truegmm, σmin = 0.4)...,
      label = L"\sigma_k = 1", linestyle=:dash, alpha = 0.7, width = 1.5)
plot!(lls_lower_σ(data.xs, truegmm, @set truegmm.comps[1].σ = 0.5)...,
      label = L"\sigma_k = 0.5", linestyle=:dash, alpha = 0.7, width = 1.5)
plot!(lls_lower_σ(data.xs, truegmm, @set truegmm.comps[1].σ = 0.4)...,
      label = L"\sigma_k = 0.4", linestyle=:dash, alpha = 0.7, width = 1.5)

savefig("em-loglike-approx.pdf")

#plot(lls_lower_σ(data.xs, falsegmm)...)

