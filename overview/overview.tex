\documentclass[11pt]{article}

\usepackage{color}
\usepackage{url}
\usepackage[margin=0.7in]{geometry}
\usepackage{enumitem}
\usepackage[colorlinks=true, bookmarks=true,  plainpages=false, pdfpagelabels=true, pageanchor=false,hypertexnames=false,linkcolor=blue,citecolor=blue,pdfauthor={Marek Petrik},urlcolor=blue]{hyperref}
\usepackage{booktabs}
%\usepackage{gfsdidot}
\usepackage[T1]{fontenc}
%\usepackage{newpxtext,mathpazo}

\setlength{\parskip}{3mm plus1mm minus1mm}
\setlength{\parindent}{0cm}

\date{}

\begin{document}
\begin{center}
	{\Large\bf CS 750/850: Machine Learning}\\
	Fall 2023\\
	\textbf{Website:} \url{https://gitlab.com/marekpetrik/ml-fall2023}
\end{center}


\section{Basic Course Information}

\begin{itemize}
\item \textbf{Department}: Computer science
\item \textbf{Course title}: CS 750 and CS 850
\item \textbf{Number of credits}: 4 for CS 750 and 3 for CS 850
\item \textbf{Semester}: Fall 2023
\item \textbf{Modality}: In person. There is no plan for zoom sessions or lecture recordings.
\end{itemize}

\section{Basic Instructor Information}

\begin{itemize}
\item \textbf{Name of the instruction}: \emph{Marek Petrik}
\item \textbf{Office address and phone number}: Kingsbury N215b and  (603) 862-2682
\item \textbf{Email address}: \url{mailto:mpetrik@cs.unh.edu}. Unless it is urgent, I will answer it during the office hours
\item \textbf{Office hours}: Please see the class website
\item \textbf{Preferred method of contact}: In person during office hours
\item \textbf{Names and contact of TA}: \emph{Jia Lin Hau}, Kingsbury W240. Please see the website for office hours.
\end{itemize}

The \emph{fastest} way to get your question about course material answered is to post it on the discussion forum listed on the website. Please do not send emails to the instructor or the TAs with questions that may be of interest to others too. If you have personal questions, it is best to come to the office hours.

\section{Description of the Course}

\paragraph{Introduction}
This class will teach you how to use machine learning to understand data and make predictions. It focuses on understanding the fundamental concepts and algorithms that underlie modern machine learning and data science.

\paragraph{Student learning outcomes}
Learn basic machine learning techniques, learn how they are derived from statistical principles, and learn how they are solved using linear algebra and mathematical optimization.

\paragraph{Format of the course}
Lectures, assignments, and online discussion.

\section{Prerequisites}

Students need to be able to code in a scripting language, such as Python, R, or Julia. Ideally the students will have also completed a course on statistics. Some experience with linear algebra will also be helpful.

\section{Course Requirements}

\subsection{Nature of assignments and exams}

\subsection{Federal credit hour requirement}

\subsection{Deadlines and test dates}

\subsection{Description of grading procedures}

\subsection{Components of final grade}

\section{Learning Resources}

\section{Contact Information}

\begin{itemize}[nosep]
	\item \textbf{Marek Petrik} (Marek).
	\begin{itemize}
		\item Email: \url{mailto:mpetrik@cs.unh.edu}
		\item Office:  Kingsbury N215b
		\item Office hours:  Please see the class website
	\end{itemize}
	\item \textbf{Jia Lin Hau} (Monkie)
	\begin{itemize}
		\item Office: Online
		\item Office hours: Please see the class website
	\end{itemize}
\end{itemize}


\section{Textbooks}

The principal reference is: \emph{James, G., Witten, D., Hastie, T., \& Tibshirani, R. (2021). An Introduction to Statistical Learning}. \textbf{second edition}. The pdf is available online.

Other references are listed on the website and all have pdf's available.

\section{Evaluation}

The weights of individual components in computing the grade are as follows:\\ [2mm]
\begin{tabular}{|r|l|}
	\toprule
	\textbf{45\%} & Homework assignments  \\
	\textbf{15\%} & 4 Quizzes \\
	\textbf{20\%} & Final exam (take-home) \\
	\textbf{20\%} & Class project \\
	\bottomrule
\end{tabular} \\[2mm]

There will be opportunities to get optional extra-credit points throughout the semester. In particular, the review sessions (mid-term and final) will have opportunities to get extra points for the final exam based on an interactive quiz. 

The final grades will be computed using the standard UNH scale.

\paragraph{Disability} If you are registered with the student disability office, please let me know right away so that I can provide proper accommodation.

\paragraph*{Academic honesty} Please, be honest. No forms of cheating or plagiarism will be tolerated. You are encouraged to talk to your classmates about the problems as well as search the internet. \textbf{However, please do not seek out solution guides, and if you come across one do not study it.} It is \textbf{not} OK to copy code from classmates, the internet, or anywhere. If you have the same variable names, exactly the same errors, the same comments as another classmate or an online source, your solution will be considered copied and you \underline{will fail the class}.  \textbf{In 2020, 5 students failed the class because of plagiarism and 3 of them had to leave UNH.}

\begin{itemize}[noitemsep]
	\item University Academic Honesty Policy:\\ \url{https://www.unh.edu/student-life/handbook/academic/academic-honesty}
	\item Tutorial on Plagiarism: \\  \url{http://cola.unh.edu/plagiarism-tutorial-0}
\end{itemize}

\paragraph*{Exceptions} If you have a serious reason for not being able to complete an assignment or a quiz, or have other concerns about succeeding in the class, please do not hesitate to contact the instructor.


\subsection{Assignments}
\textbf{Start early. Do not put it off until the last moment.}

\paragraph{General policies}

\begin{enumerate}[nosep]
	\item \textbf{Late penalty:} 5\% per \textbf{hour}. One worst grade in assignments 1-6, and another one in assignments 7-12 will be dropped.
	\item If you have a legitimate reason for why you cannot finish an assignment on time, please talk to the instructor.
	\item Assignments are due on the day and hour indicated on myCourses. Generally due on Tuesdays.
	\item Assignments should be turned in as a \textbf{PDF} and a \textbf{separate source code file} (such as RMarkdown, R, Python, or others) on myCourses. There may be no credit for submitting only source code that does not compile.
	\item Please do not copy solutions from online sources or your friends; it defeats the purpose of the assignments and you will not learn the material.
	\item We encourage you to collaborate with your classmates, but this must not involve copy-pasting. Copy-pasting from online sources is also not OK. Each homework solution should be prepared independently.
\end{enumerate}

\paragraph*{Programming Assignments}

The class will involve hands-on data analysis using machine learning methods. The recommended language for programming assignments is R which is an excellent tool for statistical analysis and machine learning. No prior knowledge of R is needed or expected; the book and lectures will include a gentle introduction to the language. Python is also an option for students who have some experience with it and are willing to figure things out on their own.

\section{Exams \& Quizzes}

The final exams will be take-home over a period of several days. It will require about 5 hours to complete and will be based on homework problems. No collaboration on the exams. There will be no midterm.

There will be 4 additional online quizzes to complete during the course of the semester. You will have at least one week to complete each one of them. Each quiz should take less than an hour.

\section{Class Projects}

The class will involve a project with multiple groups of up to 5 people working as a team. The class project will have a longer report and presentation due at the end of the semester.


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
