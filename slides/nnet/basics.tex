\documentclass{beamer}

\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{grffile}
\usepackage{amssymb}

\newcommand{\opt}{^{\star}}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\tr}{^\top}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\newenvironment{mprog}{\begin{equation}\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}\end{equation}}
\newenvironment{mprog*}{\begin{equation*}\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}\end{equation*}}
\newcommand{\stc}{\\[1ex] \subjectto}
\newcommand{\subjectto}{\mbox{s.t.} &}
\newcommand{\minimize}[1]{\min_{#1} &}
\newcommand{\maximize}[1]{\max_{#1} &}
\newcommand{\minsep}[2]{\min_{#1 \; \vline \; #2} &}
\newcommand{\maxsep}[2]{\max_{#1 \; \vline \; #2} &}
\newcommand{\cs}{\\[1ex] & }

\title{Deep Learning}
\author{Marek Petrik}
\date{Nov 21, 2023}

\begin{document}

\begin{frame} \maketitle
\end{frame}

\begin{frame}
  \frametitle{Today: Deep Learning}

  \begin{itemize}
  \item Single Layer Neural Network 
  \item Multi-Layer Neural Network
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Motivation: Image Classification}
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_5.jpg} 
  \end{center}
\end{frame}

\begin{frame} \frametitle{What is Machine Learning}
    \begin{itemize}
            \item Discover unknown function $\alert{f}$:
            \[ \tcb{Y} = \tcr{f}(\tcg{X})  \]
            \item $\tcr{f}$ = \textbf{hypothesis}, or model
            \item $\tcg{X}$ = \textbf{features}, or predictors, or inputs
            \item $\tcb{Y}$ = \textbf{response}, or target
    \end{itemize}
    \[ \varname{MPG} = \alert{f}(\varname{Horsepower}) \]
    \begin{center}\includegraphics[width=0.5\linewidth]{../figs/class1/auto_function.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{What is Deep Learning (NNets)}
\centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
            \node[block](data){Dataset};
            \node[block,below of=data](algo){Machine Learning Algorithm};
            \node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
            \node[left of=hypo,xshift=-1cm](input) {Predictors $\tcg{X}$};
            \node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
            \path (data) edge (algo)
                        (algo) edge (hypo)
                        (input) edge [dashed] (hypo)
                        (hypo) edge [dashed] (output);
    \end{tikzpicture}
    
\end{frame}

\begin{frame}
  \frametitle{History of Deep Learning}
  \emph{Deep learning}: Artificial neural networks with many layers that can learn complex representations \\
  \vfill 
  \begin{itemize}
  \item 1958: Perceptron algorithm proposed for classification to mimick a human neuron
  \item 1980s: Neural networks rise in prominence
  \item 1995--2010: Other ML developed: SVM, boosting, trees, probabilistic ML
  \item 2010--: Rise of deep learning for image, speech, text recognition and RL
  \end{itemize}
\end{frame}



\begin{frame}
  \frametitle{Empirical Risk Minimization}
 \begin{center}
 \includegraphics[width=0.8\linewidth]{../figs/statlearning/statlearning.png}
 \end{center}

 \textbf{Generalization error}:
 \begin{align*}
   R(g_n) - R(g\opt) &\le R(g_n) - R_n(g_n) + R_n(g_n) - R(g\opt)  \\
                     &\le \bigl( R(g_n) - R_n(g_n) \bigr) + \bigl( R_n(g\opt) + R(g\opt)\bigr)  \\
   &\le 2\cdot \sup_{g\in \mathcal{G}} |R(g) - R_n(g)|
\end{align*}
\end{frame}

\begin{frame} \frametitle{Generalization Error Bounds}
  \textbf{Test error} (for some $g$ independent of data) with probability $1-\delta$ is
  \[ |R(g) - R_n(g)| \le \sqrt{ \frac{\log \frac{2}{\delta }}{2n} }\]
  \vfill 
  \textbf{Training error} ($g_n$ computed from data) with probability $1-\delta$ is
  \[  |R(g_n) - R_n(g_n)| \le \sqrt{\frac{\log  |\mathcal{G}| + \log \frac{1}{\delta }}{2n}} \]
  The generalization depends on the size $|\mathcal{G}|$ of the set of possible classifiers
\end{frame}
\begin{frame} \frametitle{Artificial Neuron (Unit, Perceptron) }
  \begin{itemize}
  \item \textbf{Inputs} (features): $X_1, \dots, X_p$ 
  \item \textbf{Output} (target): $Y$
  \item \textbf{Parameters} (weights): $w_0, w_1, \dots , w_p$, or vector $w$
    \item \textbf{Activation function}: $g \colon \mathbb{R} \to \mathbb{R} $ a nonlinear function
  \end{itemize}
\vfill 
  Compute output as:
  \[
    Y = g \left( w_0 + \sum_{j = 1}^{p} w_j\cdot X_j \right) = g \left( w_0 + w\tr X \right)
  \]
\end{frame}

\begin{frame}
  \frametitle{Using Neuron}

  \textbf{Training}: Compute weights $w$ to minimize the training error (empirical risk minimization) such as classification error or RSS/MSE
  \\
  \vfill
  \textbf{Prediction}: Compute the output using the trained weights $w$
\end{frame}

\begin{frame} \frametitle{Activation Function}
  Linear function
  \[
   g(z) = z 
  \]
  Sigmoid (logistic) function (other functions possible)
  \[
   g(z) = \frac{e^z}{1 + e^z}
 \]
\vfill 
 ReLU: Rectified Linear Unit
 \[
  g(z) = [z]_+ = \max \{0, z\} 
\]
\end{frame}

\begin{frame} \frametitle{Activation Function}
  \begin{center}
  \includegraphics[width=0.8\linewidth]{../islr2figs/Chapter10/10_2.pdf}
  \end{center}
  \vfill 
\pause
 What is the purpose of an activation function?
\end{frame}

\begin{frame}
  \frametitle{Activation Function and Prediction}
  \textbf{Classification (2 classes)}: Use a sigmoid function \\
  \vfill
  \textbf{Classification (k classes)}: Use $k$ neurons with sigmoid functions, one for each class (one vs all)
  \vfill
  \textbf{Regression}: Linear or ReLU activation function
\end{frame}

\begin{frame} \frametitle{Single (hidden) Layer Neural Network}
  \begin{itemize}
  \item Input layer (features): $X_1, \dots, X_p$
    \item Hidden layer: $A_1, \dots , A_k$
  \item Output (target): $Y$
  \end{itemize}
  \begin{center}
\includegraphics[width=0.6\linewidth]{../islr2figs/Chapter10/10_1.png}
  \end{center}
  \vfill
  \pause
  Nonlinearity is essential (otherwise more layers do not add expressivity)
\end{frame}

\begin{frame} \frametitle{Single Layer Neural Net: Formula}
  \begin{center}
\includegraphics[width=0.5\linewidth]{../islr2figs/Chapter10/10_1.png}
  \end{center}
  \vfill
  \[
   Y = f(X) = \beta_0 + \sum_{k=1}^K  \beta_k \cdot \underbrace{g\left(w_{k_0} + \sum_{j=1}^{p} w_{kj} \cdot X_j\right)}_{A_K}  
  \]
  Each layer can use a different activation function
\end{frame}

\begin{frame} \frametitle{MNIST: Digit Recognition Dataset}
  
  \begin{center}
    \includegraphics[width=0.7\linewidth]{../islr2figs/Chapter10/10_3a.pdf} \\
    \vspace{0.4cm}
    \includegraphics[width=0.5\linewidth]{../islr2figs/Chapter10/10_3b.pdf}
  \end{center}
  Image size: $28 \times  28$ grayscale pixels\\
  Training set: $60,000$ images
\end{frame}

\begin{frame} \frametitle{Multilayer Neural Net}
  More than a single hidden layer
  \begin{center}
    \includegraphics[width=0.6\linewidth]{../islr2figs/Chapter10/10_4.png}
  \end{center}
  $p = ?$ and $K_1 = 256$ and $K_2 = 128$. All weights?
\end{frame}

\begin{frame}
  \frametitle{Training an MNIST Neural Net}

  \textbf{Objective}: Minimize negative log-likelihood (cross-entropy)
  \[
    -\sum_{i=1}^{n} \sum_{m=0}^{9} y_{im} \log (f_m(x_i))
  \]
\vfill 
More coefficients than data points. 
  \textbf{Regularization}: Ridge regularization or dropout
  
\end{frame}

\begin{frame}
  \frametitle{MNIST Error Rates}
  \begin{center}
    \begin{tabular}{l|r}
    Method & Test error \\
      \hline
    NN + Ridge & 2.3\% \\
    NN + Dropout & 1.8\% \\
    Logistic regression & 7.2\% \\
    LDA & 12.7\% 
  \end{tabular}
  \end{center}
\end{frame}

\begin{frame} \frametitle{Image Classification: Convolutional Neural Nets}
  Image classification for complex images
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_5.jpg} 
  \end{center}
  \textbf{CIFAR100 dataset}: $60,000$ images with $100$ classes, $32\times 32$ pixels with $3$ colors
\end{frame}

\begin{frame} \frametitle{Convolutional Neural Nets: Main Idea}
  Image composed from local features and image is location invariant
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_6.jpg} 
  \end{center}
\end{frame}

\begin{frame} \frametitle{CNN CIFAR100 Architecture}
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_8.pdf} 
  \end{center}
\end{frame}

\begin{frame} \frametitle{Convolutional Layer: Find Local Features}
  \includegraphics[width=6cm]{../islr2figs/Chapter10/oi.png}
  \vfill 
  \includegraphics[width=6cm]{../islr2figs/Chapter10/cf.png}
  \vfill 
   \includegraphics[width=11cm]{../islr2figs/Chapter10/ci.png} 
   \vfill
  Training set used to identify best covolutional filters
\end{frame}

\begin{frame} \frametitle{Example Convolution}
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_7.pdf} 
  \end{center}
   Image is often padded with 0s to deal with edges
\end{frame}

\begin{frame}
  \frametitle{Pooling Layer}
  Summarize and condense an image with location invariance 
  \begin{center}
   \includegraphics[width=0.8\linewidth]{../islr2figs/Chapter10/maxpool.png} 
  \end{center}
\end{frame}

\begin{frame} \frametitle{CNN CIFAR100 Architecture}
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_8.pdf} 
 \end{center}
 \textbf{Important}: Entire convolutional channel shares the same weights
\end{frame}

\begin{frame}
  \frametitle{Other Considerations}
  \begin{itemize}
  \item \textbf{Pre-trained models}: can be downloaded and used for new problems by training the last layer only (logistic regression)
    \vfill 
  \item \textbf{Data augmentation}: Perturb images to generate more training data
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/tiger.png} 
 \end{center}
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Results}
  \begin{center}
   \includegraphics[width=0.8\linewidth]{../islr2figs/Chapter10/results.png} 
 \end{center}
\end{frame}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End: