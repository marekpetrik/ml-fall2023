\documentclass{beamer}

\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Designing Nonlinear Features}
\subtitle{Linear regression and beyond}
\author{Marek Petrik}
\date{Oct 24, 2023}


\begin{document}

\begin{frame} \maketitle
\end{frame}

\begin{frame} \frametitle{Linear Regression}
	\begin{itemize}[<+->]
		\item Can linear regression fit non-linear functions?
		\item Can logistic regression be used to compute non-linear decision boundaries?
		\item What feature transformations do you know?
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{When to Fit Nonlinear Model?}
\begin{itemize}
	\item<2-> Residual plot
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.9}.pdf}\end{center}%
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Approaches to Nonlinear Feature Relationship}
\visible<2->{\textbf{Today:} Problems with a single variable}
\begin{itemize}
	\item We will cover:
	\begin{enumerate}
		\item Polynomial regression
		\item Step functions
		\item Regression splines
	\end{enumerate}
	\item<2-> Other significant ones:
	\begin{enumerate}
		\item Smoothing splines
		\item Local regression
		\item Generalized additive models
		\item Fourier Analysis
		\item Wavelets
	\end{enumerate}
\end{itemize}

\end{frame}

\begin{frame} \frametitle{Polynomial Regression}
\begin{itemize}
	\item Standard linear model:
	\[ y_i = \beta_0 + \beta_1 x_i + \epsilon_i \]
	\item Polynomial function:
	\[ y_i = \beta_0 + \beta_1 x_i + \beta_2 x_i^2 + \beta_3 x_i^3 + \ldots + \beta_d x_i^d + \epsilon_i \]
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Example Polynomial Regression}
\begin{itemize}
	\item Linear regression:
	\[ \varname{mpg} = \beta_0 + \beta_1 \times \varname{power} \ \]
	\item Degree $2$ (Quadratic):
	\[ \varname{mpg} = \beta_0 + \beta_1 \times \varname{power} + \beta_2 \times \varname{power}^2 \]
	\item Degree $k$:
	\[ \varname{mpg} = \sum_{i=0}^{k} \beta_k \times \varname{power}^k \]
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Polynomial Functions}
\vspace{-6mm}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.8}.pdf}\end{center}%
\vspace{-10mm}
\end{frame}


\begin{frame} \frametitle{Why Polynomial Regression is Insufficient?}
	\begin{itemize}
		\item Does not account for \textbf{local} non-linearity
		\item Limited a-priori knowledge
		\item Very unstable in extreme ranges
		\item Polynomials are not local: generalize trends to infinity
		\begin{itemize}
			\item World war 2 effect ends at 1960
			\item Earning in college not predictive of later career earnings
		\end{itemize}
	\end{itemize}

\end{frame}

\begin{frame} \frametitle{Step Functions}
	\begin{itemize}
		\item Like dummy variables for quantitative features
		\item Create cut points $c_1, c_2, \ldots, c_K$
		\item Construct $K+1$ new features:
		\begin{align*}
		C_0(X) &= I(X < c_1) \\
		C_1(X) &= I(c_1 \le X < c_2) \\
		&\vdots
		\end{align*}
		\item $I(\cdot)$ is an \textbf{indicator function} / dummy variable
		\[ I(b) = \begin{cases} 1 &\text{if } b = \operatorname{True} \\
		0 &\text{otherwise} \end{cases} ~.\]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Step Functions Example}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter7/7.2}.pdf}\end{center}%
\pause
\textbf{Step functions are not continuous!}
\end{frame}

%\begin{frame} \frametitle{Basis Functions}
%	\begin{itemize}
%		\item Polynomial functions are new \textbf{basis functions}
%		\item Step functions are new \textbf{basis functions}
%		\item \textbf{Basis Functions}: Span linear space
%		\item Linear algebra detour
%	\end{itemize}
%\end{frame}
%
%\begin{frame} \frametitle{Basis of Vector Space}
%	\begin{itemize}
%		\item Vectors $X_1, X_2, \ldots, X_K$
%		\item \textbf{Span} of vectors (space):
%		$$ \alpha_1 X_1 + \alpha_2 X_2 + \ldots + \alpha_K X_K $$
%		\item \textbf{Basis}: smallest set of vectors that spans a space
%	\end{itemize}
%\end{frame}
%
%\begin{frame} \frametitle{Column View of Linear Regression}
%	\begin{itemize}
%		\item Linear regression:
%		$$ \min_\beta \| y - X \beta\|_2^2 $$
%		\item Treat vectors as columns:
%		$$ \min_\beta \| y - X_1 \beta_1 - \ldots - X_K \beta_K \|_2^2$$
%		\item \textbf{Interpretation}: closest point to $y$ in space spanned by $X_1, \ldots, X_K$
%		\vfill
%		\pause
%		\item \textbf{Features are the basis!}
%	\end{itemize}
%\end{frame}

\begin{frame} \frametitle{Regression Splines}

	\begin{itemize}
		\item \textbf{Polynomials} are not local: generalize trends to infinity
		\begin{itemize}
			\item World war 2 effect ends at 1960
			\item Earning in college not predictive of later career earnings
		\end{itemize}
		\item \textbf{Step functions} are not continuous or smooth
		\begin{itemize}
			\item Population grows smoothly
		\end{itemize}
		\vfill
		\pause
		\item Regression splines are local and smooth
		\pause
		\item Derivation in several steps
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Step 1: Step Function as Piecewise Polynomials}
	\begin{itemize}
		\item Like Dummy variables for quantitative features
		\item Step functions (minor change in $\le$):
		\begin{align*}
			C_0(X) &= I(X < c_1) \\
			C_1(X) &= I(c_1 < X \le c_2) \\
			&\vdots
		\end{align*}
		\item Step-functions are piece-wise polynomials of degree 0
		\[ C_i(X) = \begin{cases}
			1 &\text{if } X > c_{i} \text{ and } X \le c_{i+1}\\
			0 &\text{otherwise}
		\end{cases} \]
		\item Different representation (\textbf{basis spans the same space!}):
		\[ C_i(X) = \begin{cases}
		1 &\text{if } X > c_{i-1} \\
		0 &\text{otherwise}
		\end{cases} \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Step 2: Piecewise Polynomials}
	\begin{itemize}
		\item Piecewise polynomials of degree 1:
		\[ P_i(X) = \begin{cases}
			X &\text{if } X > c_{i} \text{ and } X \le c_{i+1}\\
			0 &\text{otherwise}
		\end{cases} \]
		\item Piecewise polynomials of degree 2:
		\[ P_i(X) = \begin{cases}
			X^2 &\text{if } X > c_{i} \text{ and } X \le c_{i+1}\\
			0 &\text{otherwise}
		\end{cases} \]
		\item Piecewise polynomials of degree 3:
		\[ P_i(X) = \begin{cases}
			X^3 &\text{if } X > c_{i} \text{ and } X \le c_{i+1}\\
			0 &\text{otherwise}
		\end{cases} \]
		\pause
		\item Local but \textbf{not continuous!}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Step 3: Continuity and Regression Splines}
	\begin{itemize}
		\item Piecewise polynomials of degree 1:
			\[ P_i(X) = \begin{cases}
			X &\text{if } X > c_{i} \text{ and } X \le c_{i+1}\\
			0 &\text{otherwise}
			\end{cases} \]
		\pause
		\item \textbf{Must prevent discontinuity in knots}
		\pause
		\item Different representation:
		\[ H_i(X) = \begin{cases}
			X - c_{i} &\text{if } X > c_{i} \\
			0 &\text{otherwise} \end{cases} \]
		\pause
		\item Each feature is 0 in its knot
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{General Regression Splines}
	\begin{itemize}
		\item Regression splines of degree $d$ (such as $d=3$):
		\[ H_i(X) = \begin{cases}
			(X - c_{i})^d &\text{if } X > c_{i} \\
			0 &\text{otherwise} \end{cases} \]
		\pause
		\item Compact representation:
		\[ h(x,\xi) = ([x - \xi]_+)^d = (\max \{ x - \xi, 0\})^d \]
		\pause
		\item Most common are \textbf{cubic splines}: continuous and continuously differentiable
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Example Splines}
\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter7/7.3}.pdf}\end{center}%
\end{frame}

\begin{frame} \frametitle{Natural Splines}
Boundary segments are linear
\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter7/7.4}.pdf}\end{center}%
\end{frame}

\begin{frame} \frametitle{Natural Splines and Logistic Regression}
\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter7/7.5}.pdf}\end{center}%
\end{frame}

\begin{frame} \frametitle{Natural Splines vs Polynomials}
\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter7/7.7}.pdf}\end{center}%
\end{frame}


\begin{frame} \frametitle{Choosing Knots}
\begin{itemize}
	\item Domain dependent
	\item Change of mode
	\item Quantiles of data is generally a good choice
	\item Number of knots = degrees of freedom
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Smoothing Splines}
	\begin{itemize}
		\item Extreme version of regression splines
		\item Knot in every data point
		\pause
		\item Must have regularization to generalize
		\[ \sum_{i=1}^n (y_i - g(x_i))^2 + \lambda \int g''(t)^2 dt \]
		\item Smoothing parameter $\lambda$ chosen by LOOCV
		\item Effective degrees of freedom: technical, but not very important
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Other Methods in the Book}
	Read also 7.6 and 7.7:
	\begin{itemize}
		\item Local regression
		\item General Additive Models
	\end{itemize}
\end{frame}


\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
