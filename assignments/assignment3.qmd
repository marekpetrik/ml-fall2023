---
title: "Assignment 3"
subtitle: CS 750/850 Machine Learning
output: pdf_document
---

- **Due**: Tuesday 9/19 at 11:59PM
- **Submission**: Turn in as a **PDF** and the **source code** (qmd) on [MyCourses](http://mycourses.unh.edu). Please do not submit IPython ipynb notebooks, but convert them to qmd (see <https://quarto.org/docs/tools/jupyter-lab.html#converting-notebooks>).

Feel free to achieve the results using commands other than the ones mentioned. R shines when it comes to processing and visualization of structured data, but you would not be able to tell from the ISL book. It uses a basic subset of R. I recommend checking out `dplyr` for data processing  and GGPlot for plotting.

**Equal credit for all problems**

## Problem 1

In this exercise you will create some simulated data and will fit simple linear regression models to it. Make sure to use `set.seed(1)` [P: `np.random.seed(1)`] prior to starting part (1) to ensure consistent results.

1. Using the `rnorm()` [P: `np.random.normal`] function, create a vector, `x`, containing $100$ observations drawn from a $\mathcal{N}(0, 3)$ distribution (Normal distribution with the mean $0$ and the **standard deviation** $\sqrt{3}$). This represents a feature, $X$.
2. Using the `rnorm()` function, create a vector, `eps`, containing $100$ observations drawn from a $\mathcal{N}(0, 0.5)$ distribution i.e. a normal distribution with mean zero and standard deviation $\sqrt{0.5}$.
3. Using `x` and `eps`, generate a vector `y` according to the model $Y$:
	$$ Y = -2 + 0.6 X + \epsilon $$
What is the length (number of elements) of `y`? What are the values of $\beta_0,\beta_1$ in the equation above (intercept and slope)?
4. Create a scatterplot displaying the relationship between `x` and `y`. Comment on what you observe.
5.  Fit a least squares linear model to predict `y` using `x`. Comment on the model obtained. How do $\hat{\beta}_0,\hat{\beta}_1$  compare to $\beta_0,\beta_1$?
6. Display the least squares line on the scatterplot obtained in 4.
7. Now fit a polynomial regression model that predicts $y$ using $x$ and $x^2$. Is there evidence that the quadratic term improves the model fit? Explain your answer.

## Problem 2 
It is claimed in the ISL book that in the case of simple linear regression of $Y$ onto $X$, the $R^2$ statistic (3.17) is equal to the square of the correlation between $X$ and $Y$ (3.18). Prove that this is the case. For simplicity, you may assume that $\bar{x} = \bar{y} = 0$.

## Problem 3 

In this problem, you will derive the bias-variance decomposition of MSE as described in Eq. (2.7) in ISL. Let $f$ be the true model and $\hat{f}$ be the estimated model. Consider fixed instance $x_0$ with the label $y_0 = f(x_0)$.  For simplicity, assume that $\operatorname{Var}[\epsilon] = 0$, in which case the decomposition becomes:
$$ 
\underbrace{\mathbb{E}\left[ (y_0 - \hat{f}(x_0))^2 \right]}_{\text{test MSE}} = \underbrace{\operatorname{Var}[\hat{f}(x_0)]}_{\text{Variance}} + \left(  \underbrace{\mathbb{E}[ f(x_0) - \hat{f}(x_0) ]}_{\text{Bias}} \right)^2 ~. 
$$
Prove that this equality holds.

## Problem 4

Please work out the following problem by hand. Consider a *simple* linear regression with three data points $x_1 =1, x_2 = 2, x_3 = 3$ and labels $y_1 = 2, y_2 = 3.5, y_3 = 1$.

1. Give the precise optimization problem that one needs to solve to fit a linear regression line in this small problem.
2. Assuming that the intercept $\beta_0 = 0$ is fixed, argue that the objective function is convex in $\beta_1$.
3. Solve the optimization problem for $\beta_1$ assuming that $\beta_0 = 0$ is fixed.

## References

Each reference is a link. Please open the PDF in a viewer if it is not working on the website.

1. [R For Data Science](https://r4ds.had.co.nz/index.html)
2. [Cheatsheets](https://www.rstudio.com/resources/cheatsheets/)
