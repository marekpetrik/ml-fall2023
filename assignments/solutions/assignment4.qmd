---
title: "Assignment 4"
author: "Jia Lin, Hau (TA), modified by Sushma Anand Akoju"
date: '2023-09-21'
format: pdf 
---

- **Due**: Tuesday 9/26 at 11:59PM
- **Submission**: Turn in as a **PDF** and the **source code** (qmd) on [MyCourses](http://mycourses.unh.edu). Please do not submit IPython ipynb notebooks, but convert them to qmd (see <https://quarto.org/docs/tools/jupyter-lab.html#converting-notebooks>).


## Problem 1

Your goal in this problem is to solve for a generalized form of linear regression that allows for errors that are not distributed identically for all observations and do not have a $0$ mean. The model assumes that
$$ 
y_i = \beta_0 + \sum_{j=1}^k x_{ij} \cdot \beta_j + \epsilon_i 
$$ 
for each observation $i = 1, \ldots, n$. For each $i$, $y_i$ is the target value and $x_{ij}$ is the $j$-th feature value. The errors $\epsilon_i$ are distributed independently across observations, but not identically:
$$ 
\epsilon_i \sim \mathcal{N}(\mu_i, \sigma_i^2) 
$$
The symbol $\mathcal{N}(\mu_i, \sigma_i^2)$ denotes a normal distribution with mean $\mu_i$ and variance $\sigma_i^2$. See <https://en.wikipedia.org/wiki/Normal_distribution> for the formula that describes the PDF of this distribution. This problem assumes that $\mu_i$ and $\sigma_i$ are all known.

**1. Give the form of the likelihood of any given dataset as a function of values $\beta_j$ for the linear regression model above. **

Let define a vector of coefficient $\beta = [~\beta_0~,~\beta_1~,~\cdots.~,~\beta_k~]$,
$$p(~Y ~|~X~,~\beta~) = \prod_{i=1}^n p(~y_i~|~x_i~,~\beta~)$$


**2. Take the logarithm of the likelihood function to formulate the log-likelihood. Briefly describe how this log-likelihood function differs from the standard objective in linear regression.**

Note that our objective of maximizing likelihood is also equivalent to minimizing the negative log-likelihood:
$$\max_{\beta \in \mathbb{R}^k} ~p (~Y ~|~X~,~\beta~) = \min_{\beta \in \mathbb{R}^k} ~-p (~Y ~|~X~,~\beta~) =\min_{\beta \in \mathbb{R}^k} ~-\log \big(~p (~Y ~|~X~,~\beta~)~\big) = \min_{\beta \in \mathbb{R}^k} ~- \sum_{i=1}^n \log \big(~p(~y_i~|~x_i~,~\beta~)~\big)$$
Following the notation provided in MML, We denote the distribution of $y_i \sim \mathcal{N}(x_i^T\beta + \mu_i,\sigma_i^2)$ as $\mathcal{N}(y_i ~|~ x_i^T\beta + \mu_i,\sigma_i^2)$ . We can write each log probability as following with the information given in Q1 as
$$\log \big( ~p(~y_i~|~x_i~,~\beta~)~\big) = \log \big( ~\mathcal{N}(~y_i~|~x_i^T \beta + \mu_i~,~\sigma_i^2~)~\big) = -\frac{1}{2\sigma_i^2}(y_i - x_i^T\beta - \mu_i)^2 + c_i $$
Since the $c_i$ constant from expanding the Gaussian distribution pdf will not affect our optimization we can neglect it and derive the optimization problem as
$$\min_{\beta \in \mathbb{R}^k} ~- \sum_{i=1}^n \log \big(~p(~y_i~|~x_i~,~\beta~)~\big) =\min_{\beta \in \mathbb{R}^k} ~ \sum_{i=1}^n \frac{1}{2\sigma_i^2}(y_i - x_i^T\beta - \mu_i)^2$$

Minimizing log-likelihood is a more general form of optimization. In linear regression the noise is assumed to be normally independent identically distributed $\epsilon \sim \mathcal{N}(0,\sigma^2)$. But generally we can assume the noise to have arbitrary distribution based on our prior knowledge. Sometimes assuming Normally i.i.d error is not feasible when your residual is heterogeneous.

**3. Discuss some non-trivial choices of $\mu_i \neq 0$ and $\sigma_i^2 \neq 1$ such that this generalized model minimizes the RSS and recovers the same $\beta_j$ values as standard linear regression?**

For example, we would like to impose some information on residual $\epsilon$ w.r.t the sample size. Here, let assume we have data which identify the average profit per customer over a fixed period. Based on central limit theorem the variance of the average profit depend on the number of customer. Therefore, let assume we have predictor $x$ that denote the number of customer and $y$ denote the average profit per customer within a month, since the variance of $y$ is a function of $x$ we will have Heteroskedasticity since our standard deviation is a function of our predictor $\sigma_i = g(x_i)$. These classes of Least Square method are called the *Generalized Least Square*.

**Added by Sushma Anand Akoju, 2/21/2025**

*Generalized Least Squares*: Conditional mean of y given X is a linear function of X $y = X\beta + \epsilon$ where $\beta \in \mathbb{R^k}$

The conditional covariance epsilon given X is a non-singular matrix $Cov(\epsilon \mid X) = \Omega$

*Ordinary Least Squares*: Conditional mean $E(y \mid X] = \beta_0 + \beta_1 X$ and conditional covariance $E[\epsilon \mid X] = \sigma^2$ i.e. a constant.

Homoskedasticity vs Heteroskedasticity:
https://en.wikipedia.org/wiki/File:Hsked_residual_compare.svg

The absolute value of a residual is the non-negative difference between an observed value $y$ and corresponding predicted value $\hat{y}$ in regression, where $\epsilon_i$ is the noise at data point i. The predicted value $\hat{y}_i$ is $|\epsilon_i|$ distance away from true value $y_i$. This noise is constant in OLS and is a non-singular matrix in GLS. One way to check heteroskedasticity is to plot each $x_i$ against $f(y|x)$ and you might observe unequal noises and therefore variance is not a constant. The variance of error term is not constant across different values of independent variables i.e. for each $x_i$ indicates non-uniform residuals. Possible reasons for heteroskedasticity are: misspecified model or there are small or large number of outliers that are dominant or omitted variables etc.

**-------------------------------------------------------------------**

## Problem 2 

This problem will establish some basic properties of vectors and linear functions. You may benefit from reading Chapter 2 in the MML book linked on the website. You can use R/Python to construct and verify your solutions to some of the questions in this problem.

**1. The $L_2$ norm of a vector measures the length (size) of a vector. The $L_2$ norm for a vector $x$ of size $n$ is defined as:**
$$ \lVert x \rVert_2 = \sqrt{\sum_{i=1}^n x_i^2} $$
**Show (from the definition of the inner product) that the norm can be expressed as the following quadratic expression:**
$$ \lVert x \rVert_2^2 = x^T x ~.$$

$$x^T x = \sum_{i=1}^n x_i^2 =  (~\sqrt{\sum_{i=1}^n x_i^2}~)^2 = \lVert x\rVert^2_2$$

**2. Give an example of two square matrices $A$ and $B$ that do not commute. That is $A B \neq B A$.**

```{r}
A = matrix(c(1,2,3,4),nrow=2)
A
```

```{r}
B = matrix(c(5,6,7,8),nrow=2)
B
```

```{r}
A %*% B
```

```{r}
B %*% A
```

We can conclude that $AB \neq BA$.

**3. Give an example of two square matrices $A$ and $B$ that commute and satisfy that $A B = B A = I$ where $I$ is the identity matrix (all zeros except ones along the diagonal). The dimension of $A$ should be at least $4 \times 4$.**

```{r}
A = diag(c(1,2,3,4))
A
```

```{r}
B = solve(A)
B
```


```{r}
round(A %*% B,digits = 14)
```

```{r}
round(B %*% A,digits = 14)
```



**4. [*optional*] If the dimensions of a matrix $A$ are $5 \times 7$, can you construct a matrix $B$ such that the product $B A B^T$ is well-defined?**

This is impossible, since $A$ are $5 \times 7$ that means $B$ has to have $r \times 5$ and $B^T$ will be $5 \times r$, note that $BA$ is $r \times 7$ and $B^T$ is $5 \times r$, their inner values $7$ and $5$ would not match and therefore not possible to be well-defined. 

## Problem 3

Consider the following problem. We have a linear function $f \colon \mathbb{R}^3 \to \mathbb{R}$ defined as
$$
f(x_1,x_2,x_3) = \beta_0 + \beta_1 \cdot x_1 + \beta_2 \cdot x_2 + \beta_3 \cdot x_3~.
$$
You know the function value in 4 different points:
$$
\begin{aligned}
f(1,2,3) &= 10 \\
f(2,4,1) &= -3 \\
f(2,2,2) &= 0 \\
f(5,2,4) &= 9
\end{aligned}
$$
Can you identify the function $f$ from the information above? That is, what are the values of $\beta_0, \beta_1, \beta_2, \beta_3$?

Solve the problem as follows:

**1. Express the 4 equalities above as a system of linear equations.**
$$\beta_0 + 1 \cdot \beta_1 + 2 \cdot \beta_2 + 3\cdot \beta_3 = 10$$
$$\beta_0 + 2 \cdot \beta_1 + 4 \cdot \beta_2 + 1\cdot \beta_3 = -3$$
$$\beta_0 + 2 \cdot \beta_1 + 2 \cdot \beta_2 + 2\cdot \beta_3 = 0$$
$$\beta_0 + 5 \cdot \beta_1 + 2 \cdot \beta_2 + 4\cdot \beta_3 = 9$$

**2. Formulate this system of linear equations as**
$$A \beta = b$$
**where $\beta$ is the vector of $\beta_0, \ldots, \beta_3$ values and $A$ is an appropriately-sized matrix, and $b$ is a vector.**

\begin{align}
[[   &1   & 1 \qquad &    2&    3&    ]    &[[ \quad \beta_0  \quad&]    &= &~ 10\\
[   &1    & 2 \qquad&    4&    1&    ]    &[ \quad \beta_1 \quad &]    &= &~ -3\\
 [   &1    & 2 \qquad&    2&    2&    ]    &[ \quad \beta_2 \quad &]    &= &~ 0 \\
 [   &1    & 5 \qquad&    2&    4&    ]]   &[ \quad \beta_3 \quad &]]    &= &~ 9
\end{align}

$A\beta = b$ where $A$ and $b$ is defined below

```{r}
A = matrix(c(1,1,2,3,1,2,4,1,1,2,2,2,1,5,2,4),nrow=4,byrow=TRUE)
A
```

```{r}
b = matrix(c(10,-3,0,9),ncol=1)
b
```

**3. Solve the system of linear equations above in R or Python (or the language that you are using) solve for $\beta$**

```{r}
beta = solve(A,b)
beta
```

check solution
```{r}
round(A %*% beta,digits = 14)
```

