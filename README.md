# CS750 / CS850: Machine Learning #

### When and Where 

*Lectures*: TuTh: 9:40 am - 11:00 am

*Where*: Kingsbury N101

See [class syllabus](overview/syllabus.pdf) for the information regarding grading, rules, and office hours.

## Syllabus: Lectures

Regarding *reading materials* see the section on textbooks below. The slides will be available just before the lecture (or afterwards).

Please let me know if any links to material that we have covered are broken.

| Date   | Day | Slides                                                                                                                        | Reading               | Notebooks                                                                                                                                                                  |
|--------|-----|-------------------------------------------------------------------------------------------------------------------------------|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Aug 29 | Tue | [ML Intro](slides/intro/intro.pdf)  [Probability 1](slides/prob/prob1-notes.pdf)                                              | ISL 1,2, MML 6.1      | [intro](notebooks/intro/introduction.Rmd), [plots](slides/figs/intro/plots.R)                                                                                              |
| Aug 31 | Thu | [Probability 2](slides/prob/prob2-notes.pdf)                                                                                  | MML 6.2-3             |                                                                                                                                                                            |
| Sep 05 | Tue | [Statistics](slides/prob/stats-notes.pdf)                                                                                     | MML 6.4-5             |                                                                                                                                                                            |
| Sep 07 | Thu | [Linear regression 1](slides/linreg/linreg1.pdf)                                                                              | ISL 3.1-2             | [plots](slides/figs/class2/plots.R)                                                                                                                                        |
| Sep 12 | Tue | [Linear regression 2](slides/linreg/linreg2-notes.pdf)                                                                        | ISL 3.3-6, MML 9.2    | [LR](notebooks/linreg/linear_regression.Rmd), [bias-var](notebooks/linreg/bias_variance.Rmd), [tides](notebooks/linreg/tides.Rmd), [qq-plot](notebooks/linreg/qqplots.Rmd) |
| Sep 14 | Thu | [Basic linear algebra 1](slides/linalg/linalg1.pdf)                                                                           | MML 2.1-2.4           | See LAR                                                                                                                                                                    |
| Sep 19 | Tue | [Basic linear algebra 2](slides/linalg/linalg2.pdf)                                                                           | MML 2.5-8             | See LAR                                                                                                                                                                    |
| Sep 21 | Thu | [Linear algebra 3](slides/linalg/linalg3-notes.pdf), [LR Implementation](slides/linreg/solve.pdf)                             | MML 9.1-3             | [Source](slides/linreg/solve.Rmd)                                                                                                                                          |
| Sep 26 | Tue | [Logistic regression](slides/logreg/logreg.pdf)                                                                               | ISL 4.1-3             |                                                                                                                                                                            |
| Sep 28 | Thu | [Generative models](slides/generate/generate.pdf)                                                                             | ISL 4.4-5             |                                                                                                                                                                            |
| Oct 03 | Tue | [Generalized linear models](slides/glm/glm.pdf), [Gradient descent](slides/optimize/optimize.pdf)                             | ISL 4.6, MML 5.8, 7.1 | [Gradient](notebooks/optimize/optimization.qmd)                                                                                                                            |
| Oct 05 | Thu | [Cross-validation](slides/cv/cv.pdf)                                                                                          | ISL 5                 |                                                                                                                                                                            |
| Oct 10 | Tue | [Model selection, Lasso](slides/lasso/models.pdf)                                                                             | ISL 6.1-2             | [Double Descent](notebooks/ridge/doubledescent.jl)                                                                                                                         |
| Oct 12 | Thu | [Lasso and Bayesian ML](slides/lasso/map.pdf)                                                                                 | ISL 6.3-4, MML 9.2,3  |                                                                                                                                                                            |
| Oct 17 | Tue | Midterm review                                                                                                                |                       |                                                                                                                                                                            |
| Oct 19 | Thu | Midterm exam                                                                                                                  |                       |                                                                                                                                                                            |
| Oct 24 | Tue | [Regression splines](slides/splines/splines.pdf)                                                                              | ISL 7                 | [(poly)](notebooks/class10/polynomials.Rmd) [(splines)](notebooks/class10/spline_simple.Rmd)                                                                               |
| Oct 26 | Thu | [Decision trees and boosting](slides/trees/trees.pdf)                                                                         | ISL 8                 |                                                                                                                                                                            |
| Oct 31 | Tue | [SVC](slides/svm/svm1.pdf)                                                                                                    | ISL 9                 | [(Implementation)](notebooks/class14/separating_hyperplane.Rmd)                                                                                                            |
| Nov 02 | Thu | [SVM](slides/svm/svm2.pdf)                                                                                                    | ISL 9                 | [(Implementation)](notebooks/class14/separating_hyperplane.Rmd)                                                                                                            |
| Nov 07 | Tue | Friday. No class!                                                                                                             |                       |                                                                                                                                                                            |
| Nov 09 | Thu | [Dimensionality reduction: PCA](slides/pca/pca.pdf)                                                                           | ISL 12.1-2, MML 10    | [(PCA)](notebooks/class16)                                                                                                                                                 |
| Nov 14 | Tue | [k-means](slides/clustering/clustering.pdf)                                                                                   | ISL 12.4              |                                                                                                                                                                            |
| Nov 16 | Thu | [EM](slides/clustering/em.pdf), [Notes](slides/clustering/emnotes.pdf)                                                        | MML 11.3              | [(example)](notebooks/clustering)                                                                                                                                          |
| Nov 21 | Tue | [Neural nets](slides/nnet/basics.pdf), [Notes](slides/nnet/basicsnotes.pdf)                                                   | ISL 10.1-3            | [(keras)](notebooks/class22/keras_example.Rmd)                                                                                                                             |
| Nov 23 | Thu | Thanksgiving break!                                                                                                           |                       |                                                                                                                                                                            |
| Nov 28 | Tue | [Convolutional and recurrent neural nets](slides/nnet/convolve.pdf)                                                           | ISL 10.4-5            |                                                                                                                                                                            |
| Nov 30 | Thu | [Recommender systems and transformers](slides/nnet/transformers.pdf)                                                          |                       |                                                                                                                                                                            |
| Dec 05 | Tue | Review                                                                                                                        |                       |                                                                                                                                                                            |
| Dec 07 | Thu | [Project presentations](https://docs.google.com/spreadsheets/d/1pfH1AvQm22JxJ_RVKPqWE-5_2P3uTkxDEGt7olon9c8/edit?usp=sharing) |                       |                                                                                                                                                                            |



## Office Hours

- *Discussion*: Please use mycourses (canvas) 

- *Marek*:  Mon, Wed 2pm - 3pm in Kingsbury N215b
- *Jia Lin* (TA): Tue, Thu 2pm - 3pm in Kingsbury W244  (theory + R)
- *Ahmed Senior* (TA): Tue, Thu 1 - 2pm in Kingsbury N210 (theory + Python)

## Assignments

The assignments will be posted one week before the due date.

| Assignment                         | Source                             | Due Date             |
|------------------------------------|------------------------------------|----------------------|
| [1](assignments/assignment1.pdf)   | [1](assignments/assignment1.qmd)   | Tue 09/05 at 11:59PM |
| [2](assignments/assignment2.pdf)   | [2](assignments/assignment2.qmd)   | Tue 09/12 at 11:59PM |
| [3](assignments/assignment3.pdf)   | [3](assignments/assignment3.qmd)   | Tue 09/19 at 11:59PM |
| [4](assignments/assignment4.pdf)   | [4](assignments/assignment4.qmd)   | Tue 09/26 at 11:59PM |
| [5](assignments/assignment5.pdf)   | [5](assignments/assignment5.qmd)   | Tue 10/03 at 11:59PM |
| [6](assignments/assignment6.pdf)   | [6](assignments/assignment6.qmd)   | Tue 10/10 at 11:59PM |
| [7](assignments/assignment7.pdf)   | [7](assignments/assignment7.qmd)   | Tue 10/17 at 11:59PM |
| [8](assignments/assignment8.pdf)   | [8](assignments/assignment8.qmd)   | Tue 10/24 at 11:59PM |
| [9](assignments/assignment9.pdf)   | [9](assignments/assignment9.qmd)   | Tue 10/31 at 11:59PM |
| [10](assignments/assignment10.pdf) | [10](assignments/assignment10.qmd) | Tue 11/07 at 11:59PM |
| [11](assignments/assignment11.pdf) | [11](assignments/assignment11.qmd) | Tue 11/14 at 11:59PM |
| [12](assignments/assignment12.pdf) | [12](assignments/assignment12.qmd) | Tue 11/21 at 11:59PM |
| [13](assignments/assignment13.pdf) | [13](assignments/assignment13.qmd) | Tue 11/28 at 11:59PM |
| [14](assignments/assignment14.pdf) | [14](assignments/assignment14.qmd) | Tue 12/05 at 11:59PM |

## Project

See [Project Description](project/project.md) for the requirements and suggestions for the class project. 

| Deliverable            | Due Date             |
|------------------------|----------------------|
| 1. Introduction        | Fri 09/29 at 11:59PM |
| 2. Related work        | Fri 10/13 at 11:59PM |
| 3. Methodology         | Fri 11/03 at 11:59PM |
| 4. Preliminary results | Fri 11/17 at 11:59PM |
| 5. Final report        | Mon 12/11 at 11:59PM |


## Quizzes

There will be 4 quizzes online with the most current schedule posted on mycourses. You get multiple attempts at each quiz. 

## Exams

The exams will be in person and will be scheduled during the semester. There will be one midterm exam in the middle of the semester and one final exam during the finals period.

The exams will contain questions that are either identical to homework assignments or are very similar. There will be no surprise questions, exept possibly for extra credit.

## Textbooks ##

- **ISL**: James, G., Witten, D., Hastie, T., & Tibshirani, R. (2021).[An Introduction to Statistical Learning, (2nd)](https://www.statlearning.com/)

- **MML**: Deisenroth, Faisal, Soon, (2020) [Mathematics for Machine Learning](https://mml-book.github.io/), 

## Other Recommended Books ##

### More Machine Learning:
- **ESL**: Hastie, T., Tibshirani, R., & Friedman, J. (2009). [The Elements of Statistical Learning. Springer Series in Statistics (2nd ed.)](http://statweb.stanford.edu/~tibs/ElemStatLearn)
- **MLP**: Murphy, K (2012). Machine Learning, A Probabilistic Perspective.
- **ML**: Mitchell, T. (1997). Machine Learning
- Scholkopf B., Smola A. (2001). Learning with Kernels.
- **DL**: Goodfellow, I., Bengio, Y., & Courville, A. (2016). [Deep Learning](http://www.deeplearningbook.org/) 
- **PRM**: Bishop, C. M. (2006), [Pattern Recognition and Machine Learning](https://www.microsoft.com/en-us/research/uploads/prod/2006/01/Bishop-Pattern-Recognition-and-Machine-Learning-2006.pdf) 


### Probability and Statistics:
- **IST**: Lavine, M. (2005). [Introduction to statistical thought](http://people.math.umass.edu/~lavine/Book/book.html).
- Casella G., & Berger R. L. (2002) Statistical Inference, 2nd edition.
- **IP**: Grinstead, C., & Snell, J. (1998). [Introduction to probability](https://www.dartmouth.edu/~chance/teaching_aids/books_articles/probability_book/amsbook.mac.pdf).

### Linear Algebra:
- **LAR**: [Introductory Linear Algebra with R](http://bendixcarstensen.com/APC/linalg-notes-BxC.pdf)
- **LAO**: Hefferon, J. (2017) [Linear Algebra](http://joshua.smcvt.edu/linearalgebra/#current_version) (2017)
- **LA**: Strang, G. (2016) [Introduction to Linear Algebra](http://math.mit.edu/~gs/linearalgebra/).  *Also see:* [online lectures](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/)

### Mathematical Optimization:
- **CO** Boyd, S., & Vandenberghe, L. (2004). [Convex Optimization](http://web.stanford.edu/~boyd/cvxbook/)
- Berstsekas, D., (2016), Nonlinear programming, Athena Scientific.
- Nocedal, J., Wright, S. (2006). [Numerical Optimization](https://www.springer.com/us/book/9780387303031)


### Related Areas:
- **RL**: Sutton, R. S., & Barto, A. (2018). [Reinforcement learning](http://people.inf.elte.hu/lorincz/Files/RL_2006/SuttonBook.pdf). 2nd edition 
- **RLA**: Szepesvari, C. (2013), [Algorithms for Reinforcement Learning](https://sites.ualberta.ca/~szepesva/RLBook.html)
- **AIMA**: Russell, S., & Norvig, P. (2013). Artificial Intelligence A Modern Approach. (3rd ed.).


## Programming Language

The class will involve hand-on data analysis using machine learning methods. The recommended language for programming assignments is [R](https://www.r-project.org/) which is an excellent tool for statistical analysis and machine learning. *No prior knowledge of R is needed or expected*; the book and lectures will include a gentle introduction to the language. You can also use Python, Julia, or MATLAB instead if you are confident that you can install appropriate packages and figure out how to solve assignments. 

### R Resources

- [R For Data Science](https://r4ds.had.co.nz/index.html)
- [Cheatsheets (Markdown and others)](https://www.rstudio.com/resources/cheatsheets/)

We recommend using the free [R Studio](https://www.rstudio.com) for completing programming assignments. R Notebooks are very convenient for producing reproducible reports and we encourage you to use them.

If you are more adventurous, you may want to give nvim-r a try. It is a package for vim/nvim that has excellent R support.

### Python Resources

Book code is available for Python, for example, [here](https://github.com/JWarmenhoven/ISLR-python). If you find other good sources, please let me know. [Jupyter](https://jupyter.org/) is a similar alternative for Python. 

## Pre-requisites ##

Basic programming skills (scripting languages like Python are OK) and some familiarity with statistics and calculus. If in doubt, please email the instructor. 
