---
title: "Assignment 8"
author: "Jia Lin, Hau (TA)"
date: "2023-10-24"
output: pdf_document
---

- **Due**: Tuesday 10/24 at 11:59PM
- **Submission**: Turn in as a **PDF** and the **source code** (qmd) on [MyCourses](http://mycourses.unh.edu).


# Problem 1 

Suppose we estimate the regression coefficients in a linear regression model by minimizing
g
$$ \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2 \text{ subject to } \sum_{j=1}^p |\beta_j| \le s$$
for a particular value of $s$. For parts (1) through (5), indicate which of i. through v. is correct. **Justify** your answer.

  i.   Remain constant.
  ii.  Steadily increase. 
  iii. Steadily decrease. 
  iv.  Increase initially, and then eventually start decreasing in an inverted U shape.
  v.   Decrease initially, and then eventually start increasing in a U shape.

**1. As we increase $s$ from $0$, the training RSS will *typically*:**

Steadily decrease, this is because there is more flexibility (less constraint) on selecting $\beta$. Therefore, possibly resulting a better fit during training, which has less RSS.
  
**2. Repeat (1) for test MSE. **

Decrease initially, and then eventually start increasing in a U shape. This is because there is more flexibility (less constraint) on selecting $\beta$. Therefore, it would be beneficial at first for reducing test MSE when it's under-fit, and then it eventually start increasing when its over-fit.

**3. Repeat (1) for squared bias. **

Steadily decrease, this is because there is more flexibility (less constraint) on selecting $\beta$. Therefore, possibly resulting a better fit given the data-set and have less bias.

**4. Repeat (1) for variance. **

Steadily increase, this is because there is more flexibility (less constraint) on selecting $\beta$. Therefore, the model will have higher variance and may increase the possibility of over-fit.

**5. Repeat (1) for the irreducible error (Bayes error).**

Remain constant, irreducible error is independent from the model.

# Problem 2

In this exercise, we will predict the number of applications received using the other variables in the College (`ISLR::College`) data set.

```{r}
library(readr)
library(caTools)
library(boot)
library(leaps)
library(glmnet)
library(ISLR2)
# Definte dataset of interest
df <- College
df["Private"] = 1 * (df["Private"] == "Yes") # convert Private into numeric boolean
# Set seed so the split is always the same
set.seed(1)
split <- sample.split(df$Apps, SplitRatio = 0.8)
train_df = subset(df,split == TRUE)
test_df = subset(df,split == FALSE)
```


**1. Fit a linear model using least squares on the training set, and report the *test* error obtained.**

```{r}
lm1 = lm(Apps ~ ., data =  train_df)
ypred = predict(lm1, newdata = test_df)
MSE = mean((ypred - test_df$Apps)^2)
cat("MSE : ",MSE,"\t\t RMSE: ",sqrt(MSE))
```

**2. Use best subset selection with cross-validation. Report the test error obtained.**

```{r}
# LOOCV RMSE
cv.err = function(data){
  m1 = glm(Apps ~ .,data=data)
  cv1 = sqrt(cv.glm(data,m1)$delta[1])
  return(cv1)
}
# create regression subsets
lSl = ncol(df)-1
sets = regsubsets(Apps ~ ., data=df,nvmax = lSl)
# Run cross validation for each subset
CVerrors <- sapply(1:lSl, function(i) cv.err(df[c("Apps",names(coef(sets, id = i))[-1])]))
CVerrors
```

Best subset
```{r}
id = which(CVerrors == min(CVerrors))[1]
best.set = names(coef(sets, id = id))[-1]
best.set
cat("LOOCV-MSE:", CVerrors[id])
```

Evaluate performance on the test set we defined in (1).
```{r}
vars = c("Apps",best.set)
best.s = lm(Apps ~ ., data =  train_df[vars])
ypred = predict(best.s, newdata = test_df[vars])
best.MSE = mean((ypred - test_df$Apps)^2)
cat("MSE : ",best.MSE,"\t\t RMSE: ",sqrt(best.MSE))
```


**3. Fit a ridge regression model on the training set, with $\lambda$ chosen by cross-validation. **

```{r}
Apps_id = which(names(df) == "Apps") 
# define x,y for test and train set
y_train = train_df$Apps
x_train = as.matrix(train_df[,-Apps_id])
y_test  = test_df$Apps
x_test  = as.matrix(test_df[,-Apps_id])
# Fit Ridge Regression, find lambda with CV
ridge = cv.glmnet(x_train,y_train,alpha = 0) 
ridge.lambda = ridge$lambda.min
ridge.lambda
```

```{r}
ypred_ridge <- predict(ridge, newx=x_test, s=ridge.lambda)
ridge.MSE = mean((ypred_ridge - y_test)^2)
cat("MSE : ",ridge.MSE,"\t\t RMSE: ",sqrt(ridge.MSE))
```

**4. Fit a lasso model on the training set, with $\lambda$ chosen by cross-validation. Report the test error obtained, along with the number of non-zero coefficient estimates.**

```{r}
# Fit Lasso Regression, find lambda with CV
lasso = cv.glmnet(x_train,y_train,alpha = 1) 
lasso.lambda = lasso$lambda.min
ypred_lasso <- predict(lasso, newx=x_test, s=lasso.lambda)
lasso.MSE = mean((ypred_lasso - y_test)^2)
cat("MSE : ",lasso.MSE,"\t\t RMSE: ",sqrt(lasso.MSE))
```

```{r}
lasso.beta = predict(lasso,type="coefficients",s=lasso.lambda)
lasso.beta
```
We can see that two variables have zero coefficient therefore removed by LASSO.


**5. Briefly comment on the results obtained. How accurately can we predict the number of college applications received? Is there much difference among the test errors resulting from these approaches?**

```{r}
RMSE = sqrt(c(MSE,best.MSE,ridge.MSE,lasso.MSE))
names(RMSE) = c("standard", "best.ss","ridge","lasso")
RMSE
```

The RMSE resulting from these approaches were not much difference. However, I find it interesting to see the *performance highly dependent on the seed*. Currently we use seed 1 and ridge perform much worse than others, if we were to choose seed 42 then ridge will perform best among the methods.

# Problem 3 

It is well-known that ridge regression tends to give similar coeficient values to correlated variables, whereas the lasso may give quite different coeficient values to correlated variables. We will now explore this property in a very simple setting.

Suppose that $n = 2$, $p = 2$, $x_{11} = x_{12}$ , $x_{21} = x_{22}$. Furthermore, suppose that $y_1 + y_2 = 0$ and $x_{11} + x_{21} = 0$ and $x_{12} + x_{22} = 0$, so that the estimate for the intercept in a least squares, ridge regression, or lasso model is zero: $\hat{\beta}_0 = 0$.

**1. Write out the ridge regression optimization problem in this setting.**

$$\min_{\beta_1,\beta_2} (\sum_{i=1}^2 y_i - \beta_1 x_{i1} - \beta_2 x_{i2})^2 + \lambda (\beta_1^2+\beta_2^2)$$

**2. Argue that in this setting, the ridge coefficient estimates satisfy $\hat{\beta}_1 = \hat{\beta}_2$ .**

Since $x_{i1} = x_{i2}$, the objective above can be written as
$$\min_{\beta_1,\beta_2} (\sum_{i=1}^2 y_i - (\beta_1 + \beta_2) x_{i2})^2 + \lambda (\beta_1^2+\beta_2^2)$$
. Let fix the sum of beta as $\beta = \beta_1 + \beta_2$, and thus $\beta_2 = \beta - \beta_1$. When we minimizes the penalty part we get $\beta_1^2 + \beta_2^2 = \beta_1^2 + (\beta - \beta_1)^2 = \beta^2 - 2\beta \beta_1 + 2\beta_1^2$. Since the function is convex, we could set the derivative to zero and solve for the minimizer,
$$\frac{d}{d\beta_1} \beta^2 - 2\beta \beta_1 + 2\beta_1^2 = -2\beta + 4\beta_1 = 0$$
Therefore, the resulting $\beta_1 = \frac{1}{2}\beta$. Since $\beta_2 = \beta - \beta_1$, $\beta_2 = \beta - \frac{1}{2}\beta = \frac{1}{2}\beta$. We can therefore conclude that $\beta_1 = \beta_2 = \frac{1}{2}\beta$ regarding their sums $\beta = \beta_1 + \beta_2$.

**3. Write out the lasso optimization problem in this setting.**
$$\min_{\beta_1,\beta_2} (\sum_{i=1}^2 y_i - \beta_1 x_{i1} - \beta_2 x_{i2})^2 + \lambda ( \lvert \beta_1 \rvert+\lvert\beta_2\rvert)$$
Since $x_{i1} = x_{i2}$, the objective above can also be written as
$$\min_{\beta_1,\beta_2} (\sum_{i=1}^2 y_i - (\beta_1 + \beta_2) x_{i2})^2 + \lambda ( \lvert \beta_1 \rvert+\lvert\beta_2\rvert)$$

**4. Argue that in this setting, the lasso coefficients $\hat{\beta}_1$ and $\hat{\beta}_2$ are not unique. In other words, there are many possible solutions to the optimization problem in 3. Describe these solutions.**

Let Assume that the minimization is achieve with the sum of beta of $\beta = \beta_1 + \beta_2$. The penalty term $\lvert \beta_1 \rvert+\lvert\beta_2\rvert$ will be minimized as long as $\text{sign}(\beta_1) = \text{sign}(\beta_2)$.  Therefore any $\beta_1,\beta_2$ would be a possible optimal solution for the problem, as long as it satisfy $\lvert\beta\rvert = \lvert \beta_1 \rvert+\lvert \beta_2\rvert$. (For example, let assume the optimal optimizer is $\beta = 5$, so $\beta_1=2,\beta_2=3$ and  $\beta_1=1,\beta_2=4$ is an optimal solution but $\beta_1=6,\beta_2=-1$ is not.)
