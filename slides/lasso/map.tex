\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage[ruled,linesnumbered]{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{nicefrac}
\usepackage{grffile}
\usepackage[]{bm}

\DeclareMathOperator{\npdf}{pdf}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
\renewcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}
\newcommand{\tr}{^{T}}

\AtBeginSection[]{
	\begin{frame}
          \vfill
	\centering
	% \usebeamerfont{title}
        {\huge\bf \insertsectionhead}%
	\vfill
\end{frame}
}

\title{Bayesian ML and Lasso, Ridge Regression}
\author{Marek Petrik}
\date{Oct 12, 2022}



\begin{document}

\begin{frame} \maketitle \end{frame}

\begin{frame} \frametitle{Previously in Machine Learning}
    How to choose the right features if we have (too) many options \\
    \vfill
     Methods:
    \begin{enumerate}
    \item Subset selection
      \begin{enumerate}
      \item Best subset selection
      \item Forward stepwise selection
      \item Backward stepwise selection
      \end{enumerate}
    \item Regularization (shrinkage)
      \begin{enumerate}
      \item Ridge regression
      \item Lasso
      \end{enumerate}
    \end{enumerate}
\end{frame}


\begin{frame} \frametitle{Lasso Solutions are Sparse}
	Constrained Lasso (left) vs Constrained Ridge Regression (right)
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.7}.pdf}\end{center}
	Constraints are blue, red are contours of the objective
\end{frame}
\begin{frame} \frametitle{Why Regularization Works}
	\begin{itemize}
		\item Bias-variance trade-off
		\item Increasing $\lambda$ increases bias
		\item Example: all features relevant
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.8}.pdf}\\
		\textcolor{purple}{purple: test MSE}, black: bias, \textcolor{green}{green: variance} \\
		dotted (ridge) \end{center}
\end{frame}

\begin{frame}\frametitle{How to Choose $\lambda$?}
\begin{itemize}
	\item<2-> Cross-validation
\end{itemize}
\visible<2>{
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.12}.pdf}\end{center}}
\end{frame}

\begin{frame} \frametitle{Scale of Features Matters!}
\begin{itemize}
\item \textbf{Normalize features}: All values are between $0$ and $1$:
  \[ x_i' = \frac{x_i - x_{\min}}{x_{\max} - x_{\min}} \]
  \vfill
\item \textbf{Standardize features}: Stadard deviation is 1 and mean 0 (centering)
  \[ x_i' = \frac{x_i - \bar{x}}{\sigma_{x}} \]
  \vfill
\item Consider not standardizing features when they have the same units
\end{itemize}

\end{frame}

\begin{frame} \frametitle{Machine Learning in Everyday Life}
\centering
\includegraphics[width=\linewidth]{../figs/class6/algotoliveby.png}
\end{frame}


\begin{frame}\frametitle{Topics}
  \tableofcontents
\end{frame}

\section{Fitting ML Models}

\begin{frame} \frametitle{Principles of Machine Learning}
    How to fit a model to data?
    \begin{enumerate}
        \item \textbf{Maximum Likelihood}: Parameters that maximize the probability of observed data
        \vfill
        \item \textbf{Maximum A-Posteriori (Bayesian)}: Compute probability over model parameters. Compute most likely parameters
        \vfill 
        \item \textbf{Empirical Risk Minimization}:  Minimize some error metric on the training set (and hope it generalizes)
    \end{enumerate}

\end{frame}


\begin{frame}\frametitle{Fitting Machine Learning Models}
\begin{enumerate}
	\item \textbf{Maximum Likelihood}: linear regression, logistic regression, LDA, QDA
	\vfill
	\item \textbf{MAP}: Lasso, ridge regression, Bayesian models (Latent Dirichlet Allocation)
          \vfill 
	\item \textbf{Empirical Risk Minimization}: KNN, SVM, decision trees, NN, linear regression
\end{enumerate}
\end{frame}

\section{Maximum Likelihood}

\begin{frame} \frametitle{Notation}
  \begin{itemize}
  \item $i$ = index of the observation
  \item $x_i$ = feature of $i$-th observation
  \item $\beta_0 + \beta_1 x_i$ = prediction for $i$-th observation
  \item $Y_i$ = random target value; represents possible values
  \item $\epsilon_i = Y_i - \beta_0 + \beta_1 x_i$ = random noise of $i$-th prediction
  \item $y_i$ = observed value of $Y_i$
  \item $e_i = y_i - \beta_0 + \beta_1 x_i$ = instantiation of noise $\epsilon_i$
  \item $C$ = some constant (not always the same) independent of $\beta$
  \end{itemize}

  
\end{frame}

\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
	\begin{itemize}
		\item \textbf{Likelihood}: Probability that data is generated from a model \only<2>{(\alert{i.i.d. assumption})}  ($\Pr[\tcb{Y_i}]$ is short for $\Pr[\tcb{Y_i = y_i}]$)
		\only<1>{\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
		\only<2>{\[ \ell(\tcr{\beta_0,\beta_1}) = \Pr[\tcb{Y_1,Y_2,Y_3,\ldots} \mid \tcr{\beta_0,\beta_1}] = \prod_{i=1}^n \Pr[\tcb{Y_i} \mid \tcr{\beta_0,\beta_1}] \]}
		\item Find the most likely model:
		\only<1>{\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
		\only<2>{\[ \max_{\tcr{\beta_0,\beta_1}} \ell(\tcr{\beta_0,\beta_1}) = \max_{\tcr{\beta_0,\beta_1}} \Pr[\tcb{Y_1,Y_2,Y_3,\ldots} \mid \tcr{\beta_0,\beta_1}]  \]}
		\item Likelihood function is difficult to maximize
		\item Transform it using $\log$ (strictly increasing)
		\only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]}
		\only<2>{\[ \max_{\tcr{\beta_0,\beta_1}} \log \ell(\tcr{\beta_0,\beta_1}) =  \max_{\tcr{\beta_0,\beta_1}}  \sum_{i=1}^n \log \Pr[\tcb{Y_i} \mid \tcr{\beta_0,\beta_1}] \]}
		\item Strictly increasing transformation preserves maximizer
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Max-likelihood: Logistic Regression}
	\begin{itemize}
		\item \textbf{Parameters}:
		\[ \tcr{p_{\beta}}(X) = \frac{e^{\tcr{\beta_0}+\tcr{\beta_1} \, X}}{1+ e^{\tcr{\beta_0} + \tcr{\beta_1}\,X}}  \]
		\item \textbf{Data}: Features $x_i$ and labels $y_i$
		\item Likelihood:
		\[ \ell(\tcr{\beta_0},\tcr{\beta_1})  = \prod_{i : \tcb{y_i} =1} \tcr{p_{\beta}}(\tcb{x_i}) \prod_{i:\tcb{y_i}=0} (1-\tcr{p_{\beta}}(\tcb{x_i})) \]
		\item Log-likelihood:
		\[ \log \ell(\tcr{\beta_0},\tcr{\beta_1}) = \sum_{i: \tcb{y_i} =1} \log \tcr{p_{\beta}}(\tcb{x_i}) + \sum_{i:\tcb{y_i}=0} \log (1-\tcr{p_{\beta}}(\tcb{x_i})) \]
		\item Concave maximization problem
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Normal Distribution}
		Density function:
		\[ p(x) = \frac{1}{\sigma\sqrt{2\pi}} e^{-\frac{(x-\mu)^2}{2\sigma^2}} \]
		\vfill
		\begin{center}
			\includegraphics[width=0.8\linewidth]{../figs/class5/normal.pdf}
		\end{center}
\end{frame}

\begin{frame} \frametitle{Max-likelihood: Linear Regression}
\begin{itemize}
	\item \textbf{Parameters}:
	\[ \tcr{f_{\beta}}(x_i) = \tcr{\beta_0}+\tcr{\beta_1} \, x_i  \]
	\item \textbf{Data}: Features $\tcb{x_i}$ and labels $\tcb{y_i}$
	\item \textbf{Errors}: $\tcb{e_i} = \tcb{y_i} - \tcr{f_{\beta}}(\tcb{x_i}) = \tcb{y_i} - \tcr{\beta_0}-\tcr{\beta_1} \, \tcb{x_i}$
	\item Normally distributed errors: $\tcb{\epsilon_i} \sim \mathcal{N}(0,1)$, instantiation $e_i$
	\[ \npdf(e_i) = \frac{1}{\sqrt{2\pi}} e^{-\frac{e_i^2}{2}} \]
	\item Probability of observing an error:
	\[ \npdf(e_i) = \frac{1}{\sqrt{2\pi}} e^{-\frac{(\tcb{y_i} - \tcr{f_{\beta}}(\tcb{x_i}))^2}{2}} = \frac{1}{\sqrt{2\pi}} e^{-\frac{(\tcb{y_i} - \tcr{\beta_0}-\tcr{\beta_1} \, \tcb{x_i})^2}{2}} \]
\end{itemize} 
\end{frame}

\begin{frame} \frametitle{Max-likelihood: Linear Regression (2)}
	\begin{itemize}
		\item \textbf{Parameters}:
		\[ \tcr{f_{\beta}}(x_i) = \tcr{\beta_0}+\tcr{\beta_1} \, x_i  \]
		\item \textbf{Data}: Features $\tcb{x_i}$ and labels $\tcb{y_i}$
		\item \textbf{Errors}: $\epsilon_i = y_i - \tcr{f_{\beta}}(x_i) \sim \mathcal{N}(0,1)$
		\item Likelihood of parameters:
		\[ \ell(\tcr{\beta_0},\tcr{\beta_1})  =\prod_{i=1}^n  \npdf(e_i) =  \prod_{i=1}^n \frac{1}{\sqrt{2\pi}} e^{-\frac{(\tcb{y_i} - \tcr{\beta_0}-\tcr{\beta_1} \, \tcb{x_i})^2}{2}} \]
		\item Log-likelihood of parameters:
		\[ \log \ell(\tcr{\beta_0},\tcr{\beta_1}) = - \frac{1}{2} \sum_{i=1}^n (\tcb{y_i} - \tcr{\beta_0}-\tcr{\beta_1} \, \tcb{x_i})^2 + C \]
		\item Familiar?
	\end{itemize}
\end{frame}

\begin{frame}{Max-likelihood: Some issues}
\begin{enumerate}
\item If something is known about $\beta$, how to incorporate it? Important with small data sets.
\vfill
\item What is the confidence in the values $\beta$ that are learned? Important when using the model?
\end{enumerate}
\end{frame}

\section{MAP: Maximum A Posteriori}

\begin{frame} \frametitle{Learning from Small Data Sets}
	\emph{How many Wildcat buses are there?}
	\begin{center}
		\includegraphics[width=\linewidth]{../figs/class6/wildcat.jpg}
	\end{center}
	\pause
	\emph{A good guess is about 80.}
\end{frame}


\begin{frame}\frametitle{Bayes Theorem}
	\begin{itemize}
		\item Classification from label distributions:
		\[ \Pr[Y = k \mid X = x] = \frac{\Pr[X = x \mid Y = k] \Pr[Y = k]}{\Pr[X = x]} \]
		\item Example:
		\begin{gather*} \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance} = \$100] = \\ \frac{\Pr[\varname{balance} = \$100 \mid \varname{default} = \operatorname{yes}] \Pr[\varname{default} = \operatorname{yes}]}{\Pr[\varname{balance} = \$100]}
		\end{gather*}
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Bayesian Maximum A Posteriori (MAP) }
	\begin{enumerate}
		\item \textbf{Maximum likelihood}
		\[ \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
		\item \textbf{Maximum a posteriori estimate (MAP)}
		\[  \max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \visible<2->{=  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}} \]
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Maximum A Posteriori Estimate}
	\[
	\max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] =  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}
	\]
	\begin{itemize}
		\item \textbf{Prior}:
		\[  \Pr[\tcr{\operatorname{model}}] \]
		\item \textbf{Posterior}:
		\[ \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \]
		\item \textbf{Likelihood}:
		\[ \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \]
	\end{itemize}
\end{frame}

\begin{frame}{Estimating Coefficients: Maximum A Posteriori}
	\begin{itemize}
		\item \textbf{Posterior Probability}: Probability of the model being the true one \only<2>{(\alert{i.i.d. assumption})}
		\only<1>{\[ \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}} ]  \]}
		\only<2>{\[ \Pr[\tcr{\beta_0,\beta_1} \mid \tcb{Y_1,Y_2,Y_3,\ldots} ] = \prod_{i=1}^n \Pr[\tcb{Y_i} \mid \tcr{\beta_0,\beta_1}] \Pr[\tcr{\beta_0,\beta_1}] \]}
		\item Most likely model ($\Pr[\tcb{\operatorname{data}}]$ is constant, does not affect $\tcr{\beta}$):
		\only<1>{\begin{align*} \max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}} ] &= \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]} \\ &\propto \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \Pr[\tcr{\operatorname{model}}]  \end{align*}}
		\only<2>{\[ \max_{\tcr{\beta_0,\beta_1}} \Pr[\tcr{\beta_0,\beta_1} \mid \tcb{Y_1,Y_2,Y_3,\ldots} ] =  \max_{\tcr{\beta_0,\beta_1}} \prod_{i=1}^n \Pr[\tcb{Y_i} \mid \tcr{\beta_0,\beta_1}] \Pr[\tcr{\beta_0,\beta_1}] \]}
		\item Transform it using $\log$ (strictly increasing)
		\only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  + \log  \Pr[\tcr{\operatorname{model}}] \]}
		\only<2>{\[ \max_{\tcr{\beta_0,\beta_1}}  \sum_{i=1}^n \log \Pr[\tcb{Y_i} \mid \tcr{\beta_0,\beta_1}] + \log \Pr[\tcr{\beta_0,\beta_1} ] \]}
	\end{itemize}
\end{frame}

\begin{frame}{Posteriors for Biased Coin Estimation}
\centering

See section 3.3 in \emph{Murphy, K. (2012). Machine Learning: A Probabilistic Perspective. Machine Learning: A Probabilistic Perspective.}

\end{frame}

\begin{frame} \frametitle{MAP vs Max Likelihood}
	Computed models are the \textbf{same} when:\\
	\vfill
	\begin{enumerate}
		\item<2-> Prior is uniform. \emph{Uninformative priors}
		\item<3-> Amount of data available is large / infinite
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{MAP Advantages and Disadvantages}
\begin{itemize}
	\item \textbf{Advantages}:
	\begin{enumerate}
		\item Can use an informative \emph{prior}.
		\item Provides distribution of models, not just the most likely one
	\end{enumerate}
	\pause
	\vfill
	\item \textbf{Disadvantages}:
	\begin{enumerate}
		\item Requires a \emph{prior}. Where can we get it?
	\end{enumerate}
\end{itemize}
\vfill 
\emph{Uninformative priors}: Recall the two-envelope paradox
\end{frame}

\section{Regularization as MAP}

\begin{frame} \frametitle{Linear Regression as Maximum Likelihood}
\begin{itemize}
\item Linear model (Data $y_i, x_i$, model \tcr{$\beta$}):
\begin{align*}
\tcb{Y_i} &= \tcr{\beta_0} + x_i \tcr{\beta_1} + \epsilon_i \quad \text{random} \\
\tcb{y_i} &= \tcr{\beta_0} + x_i \tcr{\beta_1} + e_i \quad \text{instance}\\
\tcb{y_i} - \tcr{\beta_0} - x_i \tcr{\beta_1} &= e_i
\end{align*}
\item Normally distributed errors $\epsilon_i$ (mean $0$, variance $1$):
\begin{align*}
\tcr{f_{\beta}}(e_i) &= \frac{1}{\sqrt{2\, \pi}}
\exp\left(-\frac{e_i^2}{2}\right) \\
&= \frac{1}{\sqrt{2\, \pi}}
\exp\left(-\frac{(\tcb{y_i} - \tcr{\beta_0} - x_i \tcr{\beta_1})^2}{2}\right)
\end{align*}
\item Log-likelihood:
\[ \sum_{i = 1}^n \log \tcr{f_{\beta}}(e_i) = - \sum_{i = 1}^n \left(-\frac{(\tcb{y_i} - \tcr{\beta_0} - x_i \tcr{\beta_1})^2}{2}\right) + C = - \frac{1}{2}\operatorname{RSS} + C \]
\end{itemize} 
\end{frame}

\begin{frame}\frametitle{Ridge Regression as Normal Priors}
\begin{itemize}
\item Normal prior for $\tcr{\beta}$ values (mean $0$, variance $1$):
\[ g(\tcr{\beta_1}) = \frac{1}{\sqrt{2\, \pi}}
\exp\left(-\frac{\tcr{\beta_1}^2}{2}\right) \]
\pause
\item Log posterior:
\[
\log \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \propto \log \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] + \log \Pr[\tcr{\operatorname{model}}]
\]
\pause
\item Ridge regression log posterior:
\[ \sum_{i = 1}^n \log \tcr{f_{\beta}}(e_i) + \sum_{j=1}^p \log  g(\tcr{\beta_j}) = -\frac{1}{2} RSS - \frac{1}{2} \sum_{j=1}^p \beta_j^2 + C ~. \]
\pause
\item \alert{Question}: What about the regularization constant $\lambda$?
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Laplace Distribution}
  Heavier tails than normal distribution
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter6/6_11.pdf} 
  \end{center}
  Density
  \[ g(\tcg{x}) = \frac{1}{2b} \exp \left( \frac{|\tcg{x}-\mu |}{b} \right) \]
\end{frame}


\begin{frame}\frametitle{Lasso as Laplace Priors}
\begin{itemize}
\item Laplace prior for $\tcr{\beta}$ values (mean $0$, scale $b=1$):
  \[
    g(\tcr{\beta_1}) = \frac{1}{2} \exp \left( \frac{|\tcr{\beta_1} |}{1} \right) 
\]
\pause
\item Log posterior:
\[
\log \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] = \log \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] + \log \Pr[\tcr{\operatorname{model}}] + C
\]
\pause
\item Lasso log posterior:
\[ \sum_{i = 1}^n \log \tcr{f_{\beta}}(\tcr{\epsilon_i}) + \sum_{j=1}^p \log g(\tcr{\beta_j}) = - \frac{1}{2} RSS - \sum_{j=1}^p |\beta_j| + C~. \]
\end{itemize}
\end{frame}

\section{Bayesian Linear Regression}

\begin{frame} \frametitle{Bayesian Linear Regression} \framesubtitle{MML 9.3}
  \textbf{Notation}: Vectors bold lowercase, Matrices bold upper case, random variables tilde to distinguish from matrices
  \vfill 
  \textbf{Data}: $\bm{X}, \bm{y}$, prediction $\bm{\tilde{y}}$
  \vfill 
  \textbf{Prior}: Normal multivariate:
  \[
    \bm{\tilde{\beta}} \sim \mathcal{N}(\bm{\mu}_0, \bm{\Sigma}_0)
  \]
  \textbf{Observation errors}: are normal (standard assumption)
  \[
   y_i - \tilde{y}_i \sim \mathcal{N}(0, \sigma)
  \]
  \textbf{Posterior}: $\bm{\tilde{\beta}} \mid (\bm{X}, \bm{y}) \sim
    \mathcal{N}(\bm{\mu}, \bm{\Sigma})$ is also \textbf{normal} with
  \[
    \bm{\mu} = \bm{\Sigma}(\sigma^{-2}\bm{X}\tr \bm{y} + \bm{\Sigma}_0^{-1}\bm{\mu}_0), \quad
    \bm{\Sigma} = (\sigma^{-2} \bm{X}\tr \bm{X} + \bm{\Sigma}_0^{-1})^{-1}
  \]
  And $\bm{\tilde{y}}$ is normal too
\end{frame}

\begin{frame}
  \frametitle{Bayesian Linear Regression: Posterior}
  \centering
  Degree 3 polynomials
 \includegraphics[width=\linewidth]{../mmlfigs/blr_poly3.png} 
\end{frame}


\begin{frame}
  \frametitle{Bayesian Linear Regression: Posterior}
  \centering
  Degree 5 polynomials
 \includegraphics[width=\linewidth]{../mmlfigs/blr_poly5.png} 
\end{frame}

\begin{frame}
  \frametitle{Bayesian Linear Regression: Posterior}
  \centering
  Degree 7 polynomials
 \includegraphics[width=\linewidth]{../mmlfigs/blr_poly7.png} 
\end{frame}

\begin{frame} \frametitle{Gaussian Processes}
Bayesian linear regression with kernels
\begin{center}
	\includegraphics[width=0.8\linewidth]{../figs/class19/chi_feng_gp_regression.png}
	{\tiny Source:\url{https://livingthing.danmackinlay.name/gaussian_processes.html}}
\end{center}
\end{frame}

\section{Solving Ridge Regression}

\begin{frame} \frametitle{Linear Regression: RSS as $L_2$ Norm}
  
Linear algebra prediction ($X$ is design matrix):
\[ y = X \beta \]

RSS can be written much more compactly:
 
\begin{align*}
\operatorname{RSS} &= \| y - X \beta \|_2^2 = \\
&= (y-X\beta)\tr  (y - X \beta) = \\
&= y\tr  y - 2 y\tr  X \beta + \beta\tr  X\tr  X \beta 
\end{align*}
\end{frame}

\begin{frame}
  \frametitle{Linear Regression: Minimizing RSS}
  
Linear regression chooses $\beta$ to minimize the RSS \\
Solve the following optimization problem:
\[ \min_\beta \| y - X \beta \|_2^2 \]
Set the gradient to 0:

\begin{align*}
\nabla_\beta \;\| y - X \beta \|_2^2 &= 0 \\
\nabla_\beta \; \Bigl( y\tr  y - 2 y\tr  X \beta + \beta\tr  X\tr  X \beta \Bigr) &= 0 \\
\nabla_\beta \; \Bigl( - 2 y\tr  X \beta + \beta\tr  X\tr  X \beta \Bigr) &= 0 \\
 - 2 X\tr  y + 2  X\tr  X \beta&= 0 \\
 X\tr  X \beta &= X\tr  y 
\end{align*}
Solve a system of linear equations
\end{frame}


\begin{frame} \frametitle{Ridge Regression}
Linear algebra prediction:
\[ y = X \beta \]
\vfill 
Ridge regression objective ($I$ is the identity matrix)
\begin{align*}
\operatorname{RSS} + \lambda \cdot  \sum_{j=i}^p \beta_j^2 &= \| y - X \beta \|_2^2 + \lambda \cdot \| \beta \|_2^2 = \\
&= (y-X\beta)\tr  (y - X \beta) + \lambda \cdot \beta\tr  \beta = \\
&= y\tr  y - 2 y\tr  X \beta + \beta\tr  (X\tr  X + \lambda I) \beta 
\end{align*}
The rest follows as in linear regression except use $X\tr X + \lambda I$ in place of $X\tr X$
\end{frame}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
