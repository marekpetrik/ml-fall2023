---
title: "Assignment 4"
subtitle: CS 750/850 Machine Learning
output: pdf_document
---

- **Due**: Tuesday 9/26 at 11:59PM
- **Submission**: Turn in as a **PDF** and the **source code** (qmd) on [MyCourses](http://mycourses.unh.edu). Please do not submit IPython ipynb notebooks, but convert them to qmd (see <https://quarto.org/docs/tools/jupyter-lab.html#converting-notebooks>).


## Problem 1

Your goal in this problem is to solve for a generalized form of linear regression that allows for errors that are not distributed identically for all observations and do not have a $0$ mean. The model assumes that
$$ 
Y_i = \beta_0 + \sum_{j=1}^k x_{ij} \cdot \beta_j + \epsilon_i 
$$ 
for each observation $i = 1, \ldots, n$. For each $i$, $y_i$ is the observed target value and $x_{ij}$ is the $j$-th feature value. The errors $\epsilon_i$ are distributed independently across observations, but not identically:
$$ 
\epsilon_i \sim \mathcal{N}(\mu_i, \sigma_i^2) 
$$
The symbol $\mathcal{N}(\mu_i, \sigma_i^2)$ denotes a normal distribution with mean $\mu_i$ and variance $\sigma_i^2$. See <https://en.wikipedia.org/wiki/Normal_distribution> for the formula that describes the PDF of this distribution. This problem assumes that $\mu_i$ and $\sigma_i$ are all known.

1. Give the form of the likelihood of any given dataset as a function of values $\beta_j$ for the linear regression model above.

2. Take the logarithm of the likelihood function to formulate the log-likelihood. Briefly describe how this log-likelihood function differs from the standard objective in linear regression.

3. Discuss some non-trivial choices of $\mu_i \neq 0$ and $\sigma_i^2 \neq 1$ such that this generalized model minimizes the RSS and recovers the same $\beta_j$ values as standard linear regression? 

*Hint*: To solve this problem, you can follow the derivation from the slides on linear regression. You can also use MML Section 9.2. The result in part 2 of this problem should resemble equation (9.10a) in MML. There is no need to derive the equation with matrices and vectors.

## Problem 2 

This problem will establish some basic properties of vectors and linear functions. You may benefit from reading Chapter 2 in the MML book linked on the website. You can use R/Python to construct and verify your solutions to some of the questions in this problem.

1. The $L_2$ norm of a vector measures the length (size) of a vector. The $L_2$ norm for a vector $x$ of size $n$ is defined as:
$$ \lVert x \rVert_2 = \sqrt{\sum_{i=1}^n x_i^2} $$
Show (from the definition of the inner product) that the norm can be expressed as the following quadratic expression:
$$ \lVert x \rVert_2^2 = x^T x ~.$$

2. Give an example of two square matrices $A$ and $B$ that do not commute. That is $A B \neq B A$. 

3. Give an example of two square matrices $A$ and $B$ that commute and satisfy that $A B = B A = I$ where $I$ is the identity matrix (all zeros except ones along the diagonal). The dimension of $A$ should be at least $4 \times 4$.

4. [**optional**] If the dimensions of a matrix $A$ are $5 \times 7$, can you construct a matrix $B$ such that the product $B A B^T$ is well-defined?

## Problem 3

Consider the following problem. We have a linear function $f \colon \mathbb{R}^3 \to \mathbb{R}$ defined as
$$
f(x_1,x_2,x_3) = \beta_0 + \beta_1 \cdot x_1 + \beta_2 \cdot x_2 + \beta_3 \cdot x_3~.
$$
You know the function value in 4 different points:
$$
\begin{aligned}
f(1,2,3) &= 10 \\
f(2,4,1) &= -3 \\
f(2,2,2) &= 0 \\
f(5,2,4) &= 9
\end{aligned}
$$
Can you identify the function $f$ from the information above? That is, what are the values of $\beta_0, \beta_1, \beta_2, \beta_3$?

Solve the problem as follows:

1. Express the 4 equalities above as a system of linear equations.

2. Formulate this system of linear equations as
$$ A \beta = b~,$$
where $\beta$ is the vector of $\beta_0, \ldots, \beta_3$ values and $A$ is an appropriately-sized matrix, and $b$ is a vector.

3. Solve the system of linear equations above in R or Python (or the language that you are using)


