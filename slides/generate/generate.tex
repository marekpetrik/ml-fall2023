\documentclass{beamer}

\let\val\undefined

\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{nicefrac}
\usepackage{multirow}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{LDA, QDA, Naive Bayes}
\subtitle{Generative Classification Models}
\author{Marek Petrik}
\date{September 28, 2023}


\AtBeginSection[]{
	\begin{frame}
          \vfill
	\centering
	% \usebeamerfont{title}
        {\huge\bf \insertsectionhead}%
	\vfill
\end{frame}
}


\newcommand{\plus}{\textcolor{green}{$\mathbf{+}$}}
\newcommand{\minus}{\textcolor{red}{$\mathbf{-}$}}

\begin{document}

\begin{frame} \maketitle
\end{frame}


\section{Overview: Logistic Regression}

\begin{frame} \frametitle{Logistic Regression}
	\begin{itemize}
		\item Predict \textbf{probability} of a class: $p(X)$
		\item Example: $p(\varname{balance})$ probability of default for person with $\varname{balance}$
		\item \textbf{Linear regression}:
		\[ p(X) = \beta_0 + \beta_1\, X  \]
		\item \textbf{Logistic regression}:
		\[ p(X) = \frac{e^{\beta_0+\beta_1 \, X}}{1+ e^{\beta_0 + \beta_1\,X}}  \]
		\item Linear model of log-odds:
		\[ \log\left( \frac{p(X)}{1-p(X)}\right) = \beta_0 + \beta_1\,X \]
		\item \textbf{Odds}: $\nicefrac{p(X)}{1-p(X)}$
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Logistic Regression}
	\[ \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}]  = \frac{e^{\beta_0 + \beta_1 \varname{balance} } }{ 1+ e^{\beta_0 + \beta_1 \varname{balance} } } \]
	\vfill
	\begin{center}
		Linear regression \hspace{20mm} Logistic regression\\
		\vspace{-7mm}
		\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.2}.pdf}
	\end{center}
\end{frame}

        \begin{frame} \frametitle{Multinomial Logistic Regression}
          \begin{itemize}
          \item Multiple classes: $1:\varname{stroke}, 2:\varname{overdose}, 3: \varname{seizure}$
          \item Designate a \emph{baseline} class, such as $\varname{seizure}$
          \item Represent odds with respect to baseline class:
            \begin{align*}
\log \left( \frac{\Pr[Y = \varname{stroke}\mid X = x]}{\Pr[Y = \varname{seizure}\mid X = x]} \right) &= \beta_{\varname{stroke}, 0} + \beta_{\varname{stroke},1} \cdot x_1 \\
                \log \left( \frac{\Pr[Y = \varname{overdose} \mid X = x]}{\Pr[Y = \varname{seizure}\mid X = x]}  \right) &= \beta_{\varname{overdose}, 0} + \beta_{\varname{overdose},1} \cdot x_1 
\end{align*} 
\pause
\item Caution: The meaning of coefficients $\beta$ is relative to the \emph{baseline} class
          \end{itemize}
        \end{frame}


\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
\begin{itemize}
    \item \textbf{Likelihood}: Probability that data is generated from a model \only<2>{(\alert{i.i.d. assumption})}
    \only<1>{\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
    \only<2>{\[ \ell(\tcr{\beta_0,\beta_1}) = \Pr[\tcb{Y_1=y_1,Y_2=y_2,\ldots} \mid \tcr{\beta}] = \prod_{i=1}^n \Pr[\tcb{Y_i=y_i} \mid \tcr{\beta}] \]}
    \item Find the most likely model:
    \only<1>{\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
    \only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \ell(\tcr{\beta_0,\beta_1}) = \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \Pr[\tcb{Y_1=y_1,Y_2=y_2,\ldots} \mid \tcr{\beta_0,\beta_1}]  \]}
    \item Likelihood function is difficult to maximize
    \item Transform it using $\log$ (strictly increasing)
    \only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]}
    \only<2>{\[ \max_{\tcr{\beta_0,\beta_1}} \log \ell(\tcr{\beta_0,\beta_1}) = \max_{\tcr{\beta_0,\beta_1}} \sum_{i=1}^n \log \Pr[\tcb{Y_i = y_i} \mid \tcr{\beta_0,\beta_1}] \]}
    \item Strictly increasing transformation preserves maximizer
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Today}
    \Large
    \begin{enumerate}
        \item \textbf{Generative and Discriminative Models}
        \vfill
        \item Generative Models: LDA
        \vfill
        \item Generative Models: QDA
        \vfill
        \item Generative Models: Naive Bayes
        \vfill
        \item Which model is the right one?
        \vfill
        \item Confusion Matrix
    \end{enumerate}
  \end{frame}

  \section{Discriminative and Generative Models}

\begin{frame}\frametitle{Discriminative vs Generative Models}
	\begin{itemize}
		\item \textbf{Discriminative models}
		\begin{itemize}
                    \item Estimate conditional models $\Pr[Y \mid X]$
                    \item Linear regression
                    \item Logistic regression
                    \item K-NN
		\end{itemize}
		\vfill
		\item \textbf{Generative models}
		\begin{itemize}
                    \item Estimate joint probability $\Pr[Y, X] = \Pr[X \mid Y] \Pr[Y]$
                    \item Estimates not only the probability of labels but also the features
                    \item Once a model is fit, can be used to generate data
                    \item LDA, QDA, Naive Bayes
                    \item Early precursor of generative AI
		\end{itemize}
	\end{itemize}
\end{frame}

%\begin{frame} \frametitle{Discriminative Machine Learning}
%	\centering
%	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
%	\node[block](data){Dataset};
%	\node[block,below of=data](algo){ML Algorithm};
%	\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
%	\node[left of=hypo,xshift=-1cm](input) {Features $\tcg{X}$};
%	\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
%	\path (data) edge (algo)
%	(algo) edge (hypo)
%	(input) edge [dashed] (hypo)
%	(hypo) edge [dashed] (output);
%	\end{tikzpicture}
%\end{frame}
%
%\begin{frame} \frametitle{Generative Machine Learning}
%\centering
%	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
%	\node[block](data){Dataset};
%	\node[block,below of=data](algo){ML Algorithm};
%	\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
%	\node[left of=hypo,xshift=-1cm](input) {Features $\tcg{X}$};
%	\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
%	\path (data) edge (algo)
%	(algo) edge (hypo)
%	(hypo) edge [dashed] (input)
%	(output) edge [dashed] (hypo);
%	\visible<2->{
%		\node[block,right of=algo,xshift=1cm](prior){Class prior $\tcr{\pi}$};
%		\path (algo) edge (prior)
%			  (prior) edge [dashed] (output);
%	}
%	\end{tikzpicture}
%\end{frame}

\begin{frame}{Examples of Learned Hypotheses}
\begin{enumerate}
	\item Log odds of defaulting increases linearly proportionally to higher balance and lower income:
	\[ \log\left( \frac{p(\varname{default})}{1 - p(\varname{default})} \right) = 1 + 2.1 \times \varname{balance} - 3.1 \times \varname{income} \]
	\visible<2->{\textbf{discriminative}}
	\item $20\%$ of customers default. The average balance is $\$ 1,000$ for those who \alert{default} and $\$ 500$ for those who do not. The balances are normally distributed.
	\[ p(\varname{default}) = 0.2, \quad \begin{matrix}
		p(\varname{balance}~|~ \varname{default} = yes) \sim \mathcal{N}(1000, 10) \\		p(\varname{balance}~|~ \varname{default} = no) \sim \mathcal{N}(500, 10) \\
	\end{matrix}   \]
	\visible<2->{\textbf{generative}}
\end{enumerate}

\end{frame}

\begin{frame} \frametitle{Discriminative Machine Learning}
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
\node[block](data){Dataset};
\node[block,below of=data](algo){ML Algorithm};
\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
\node[left of=hypo,xshift=-1cm](input) {Features $\tcg{X}$};
\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
\path (data) edge (algo)
(algo) edge (hypo)
(input) edge [dashed] (hypo)
(hypo) edge [dashed] (output);
\end{tikzpicture}
\end{frame}

\begin{frame} \frametitle{Generative Machine Learning}
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
\node[block](data){Dataset};
\node[block,below of=data](algo){ML Algorithm};
\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
\node[left of=hypo,xshift=-1cm](input) {Features $\tcg{X}$};
\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
\path (data) edge (algo)
(algo) edge (hypo)
(hypo) edge [dashed] (input)
(output) edge [dashed] (hypo);
\visible<2->{
	\node[block,right of=algo,xshift=1cm](prior){Class prior $\tcr{\pi}$};
	\path (algo) edge (prior)
	(prior) edge [dashed] (output);
}
\end{tikzpicture}
\end{frame}

\begin{frame}\frametitle{Pros and Cons of Generative Models}
\begin{itemize}
	\item[\plus] Can be used to generate data ($\Pr[X]$)
	\item[\plus] Can be simpler than discriminative ones (e.g. text)
	\item[\plus] Offers more insights into data
	\item[\plus] Better performance in low-data scenarios
	\item[\plus] Can deal with missing data
	\item[\minus] Usually requires more assumptions
	\item[\minus] May not work as well when assumptions are violated
	\item[\minus] Can be computationally intensive
\end{itemize}
\end{frame}


  \section{Generative Models: LDA}

\begin{frame}\frametitle{Normal Distribution}
	\begin{itemize}
		\item Density function:
		\[ p(x) = \frac{1}{\sigma\sqrt{2\pi}} e^{-\frac{(x-\mu)^2}{2\sigma^2}} \]
		\begin{center}
			\includegraphics[width=0.5\linewidth]{../figs/class5/normal.pdf}
		\end{center}
		\item<2-> \textbf{Central limit theorem}: $Z = \nicefrac{1}{n} \sum_{i=1}^n X_i$ for i.i.d. $X_i$ is normal with $n\rightarrow \infty$
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{LDA: Linear Discriminant Analysis}
	\begin{itemize}
		\item \textbf{Generative model}: capture probability of predictors for each label
    	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.4}.pdf}\end{center}
		\item Predict:
		\begin{enumerate}
			\item<2-> $\Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ]$ and $\Pr[\varname{default} = \operatorname{yes}]$
			\item<3-> $\Pr[\varname{balance} \mid \varname{default} = \operatorname{no} ]$ and $\Pr[\varname{default} = \operatorname{no}]$
		\end{enumerate}
		\item<4-> Classes are normal: $\Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ]$
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{LDA vs Logistic Regression}
	\begin{itemize}
		\item \textbf{Logistic regressions}:
			\[ \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] \]
		\vfill
		\item \textbf{Linear discriminant analysis}:
			\[ \Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ] \text{ and } \Pr[\varname{default} = \operatorname{yes}] \]
			\[ \Pr[\varname{balance} \mid \varname{default} = \operatorname{no} ] \text{ and } \Pr[\varname{default} = \operatorname{no}] \]
	\end{itemize}
\end{frame}

\begin{frame}{Generative Models in Text Classification}
	\begin{itemize}
		\item \textbf{Goal}: Decide if a document is about \emph{physics} or \emph{chemistry}
		\vfill
		\item \textbf{Features}: Bag of words, number of times a particular word is present (reaction, molecule, force)
		\vfill
		\item A generative model?
		\vfill
		\item A discriminative model?
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{LDA with 1 Feature}
	\begin{itemize}
		\item Classes are normal and class probabilities $\pi_k$ are scalars
		\[ f_k(x) = \frac{1}{\sigma\sqrt{2\pi}} e^{  - \frac{1}{2\sigma^2} (x-\mu_k)^2 } \]
		\item \textbf{Key Assumption}: Class variances $\sigma_k^2$	\underline{are the same}, means are different.
		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.4}.pdf}\end{center}
	\end{itemize}

\end{frame}

\begin{frame}\frametitle{Bayes Theorem}
	\begin{itemize}
		\item Classification from label distributions:
			\[ \Pr[Y = k \mid X = x] = \frac{\Pr[X = x \mid Y = k] \Pr[Y = k]}{\Pr[X = x]} \]
		\item<2-> Example:
			\begin{gather*} \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance} = \$100] = \\ \frac{\Pr[\varname{balance} = \$100 \mid \varname{default} = \operatorname{yes}] \Pr[\varname{default} = \operatorname{yes}]}{\Pr[\varname{balance} = \$100]}
			\end{gather*}
		\item<3-> Notation:
			\[ \Pr[Y = k \mid X = x] = \frac{f_k(x) \pi_k}{\sum_{l=1}^{K} \pi_l f_l(x)} \]
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Classification With LDA}
	$X: \varname{balance}$, $Y: \varname{default} \in \{ \operatorname{yes}, \operatorname{no}\}$
	\begin{align*}
	\text{Probability in class } \tcg{k_1} &> \text{Probability in class } \tcr{k_2} \\
	\visible<2->{\Pr[Y = \tcg{k_1} \mid X = x] &> \Pr[Y = \tcr{k_2} \mid X = x] \\}
	\visible<3->{\frac{\pi_{\tcg{k_1}} f_{\tcg{k_1}}(x)}{\sum_{l=1}^{K} \pi_l f_l(x)} &> \frac{\pi_{\tcr{k_2}} f_{\tcr{k_2}}(x)}{\sum_{l=1}^{K} \pi_l f_l(x)} \\}
	\visible<4->{\pi_{\tcg{k_1}} f_{\tcg{k_1}}(x) &> \pi_{\tcr{k_2}} f_{\tcr{k_2}}(x) \\}
	\visible<5->{\log \left(\pi_{\tcg{k_1}} f_{\tcg{k_1}}(x) \right) &> \log\left(\pi_{\tcr{k_2}} f_{\tcr{k_2}}(x) \right) \\}
	\visible<6->{\hat{\delta}_{\tcg{k_1}}(x) &>  \hat{\delta}_{\tcr{k_2}}(x) }
	\end{align*}
	\visible<6->{
		\textbf{Discriminant function}:
		\[\hat{\delta}_{\tcb{k}}(x) = x\cdot \frac{\hat{\mu}_{\tcb{k}}}{\hat{\sigma}^2} - \frac{\hat{\mu}_{\tcb{k}}^2}{2\hat{\sigma}^2} + \log(\hat{\pi}_{\tcb{k}}) \]
		\emph{Derive at home}
	}
\end{frame}

\begin{frame}\frametitle{Estimating LDA Parameters}
	\begin{itemize}
		\item<2-> Maximum log likelihood!
		\begin{gather*}
		\max_{\mu,\sigma}\; \log \ell(\mu,\sigma) =  \max_{\mu,\sigma}\; \sum_{i=1}^N \log\left( f_{y_i}(x_i) \right) = \\
		\max_{\mu,\sigma}\; \sum_{i=1}^N \log\left( \frac{1}{\sigma\sqrt{2\pi}} \exp\left(  - \frac{1}{2\sigma^2} (x_i-\mu_{y_i})^2 \right) \right) = \\
		\max_{\mu,\sigma}\; \sum_{i=1}^N \left(-\log \sigma - \frac{1}{2\sigma^2} (x_i-\mu_{y_i})^2 + \operatorname{consts} \right)
		\end{gather*}
		\item<3-> Concave in $\mu$ and $\nicefrac{1}{\sigma^2}$, consider a single class with mean $\mu$
		\begin{align*}
		\frac{\partial}{\partial \mu} \log \ell(\mu,\sigma) &= \frac{1}{\sigma^2} \sum_{i=1}^N (x_i - \mu) = 0 \\
		\frac{\partial}{\partial \sigma} \log \ell(\mu,\sigma) &= \frac{N}{\sigma} + \frac{1}{\sigma^3} \sum_{i=1}^N (x_i - \mu)^2 = 0
		\end{align*}
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimating LDA Parameters}
\begin{itemize}
	\item $\log \ell$ is derivatives:
	\begin{align*}
	\frac{\partial}{\partial \mu} \log \ell(\mu,\sigma) &= \frac{1}{\sigma^2} \sum_{i=1}^N (x_i - \mu) = 0 \\
	\frac{\partial}{\partial \sigma} \log \ell(\mu,\sigma) &= \frac{N}{\sigma} + \frac{1}{\sigma^3} \sum_{i=1}^N (x_i - \mu)^2 = 0
	\end{align*}
	\item Therefore:
	\begin{align*}
	\mu  &= \frac{1}{N} \sum_{i=1}^N x_i \\
	\sigma^2 &= \frac{1}{N} \sum_{i=1}^N (x_i - \mu)^2
	\end{align*}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Better Parameter Estimates}
	\begin{itemize}
		\item Maximum likelihood variance $\sigma^2$ is biased:
		\begin{align*}
		\mu  &= \frac{1}{N} \sum_{i=1}^N x_i \\
		\sigma^2 &= \frac{1}{N} \sum_{i=1}^N (x_i - \mu)^2
		\end{align*}
		\item Unbiased estimate:
		\begin{align*}
		\mu  &= \frac{1}{N} \sum_{i=1}^N x_i \\
		\sigma^2 &= \frac{1}{N-1} \sum_{i=1}^N (x_i - \mu)^2
		\end{align*}
		\item \emph{See ISL for precise formula for more classes}
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{LDA with Multiple Features}
	\begin{itemize}
		\item Multivariate normal Distributions:
		\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter4/4.5}.pdf}\end{center}
		\item Multivariate normal distribution density (mean vector $\mu$, covariance matrix $\Sigma$):
		\[ p(X) = \frac{1}{(2\pi)^{p/2} |\Sigma|^{\nicefrac{1}{2}}} \exp \left(-\frac{1}{2} (x-\mu)^\top\Sigma^{-1} (x-\mu) \right) \]
	\end{itemize}
\end{frame}

\newcommand{\Tr}{\operatorname{Trace}}

\begin{frame}\frametitle{Multivariate Maximum Likelihood}
	\begin{itemize}
	\item Consider a single class:
	\begin{tiny}
		\begin{gather*}
		\max_{\mu,\Sigma}\; \log \ell(\mu,\Sigma) =  \max_{\mu,\Sigma}\; \sum_{i=1}^N \log\left( f_k(x_i) \right) = \\
		\max_{\mu,\Sigma}\; \sum_{i=1}^N \log\left( \frac{1}{(2\pi)^{p/2} |\Sigma|^{\nicefrac{1}{2}}} \exp \left(-\frac{1}{2} (x_i-\mu)^\top\Sigma^{-1} (x_i-\mu) \right) \right) = \\
		\max_{\mu,\Sigma}\; - \frac{N}{2} \log |\Sigma| - \frac{1}{2} \sum_{i=1}^N  (x_i-\mu)^\top\Sigma^{-1} (x_i-\mu) = \\
		\max_{\mu,\Sigma}\; - \frac{N}{2} \log |\Sigma| - \frac{1}{2}  \Tr\left(  \Sigma^{-1} \sum_{i=1}^N   (x_i-\mu)^\top (x_i-\mu) \right)
		\end{gather*}
	\end{tiny}
	\item Use $\nicefrac{\partial}{\partial \Sigma} \log |\Sigma| = \Sigma^{-\top}$ and $\nicefrac{1}{\partial A} \Tr(A B) = B^\top$
	\[ \Sigma = \frac{1}{N} \sum_{i=1}^N   (x_i-\mu)^\top (x_i-\mu) \]
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Multivariate Classification Using LDA}
	\begin{itemize}
		\item \textbf{Linear}: Decision boundaries are linear
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.6}.pdf}\end{center}
\end{frame}

\section{Generative Models: QDA}

\begin{frame}\frametitle{QDA: Quadratic Discriminant Analysis}
\begin{itemize}
	\item Generalizes LDA
	\vfill
	\item \textbf{LDA}: Class variances $\Sigma_k = \Sigma$ \underline{are the same}
	\item \textbf{QDA}: Class variances $\Sigma_k$ \underline{can differ}
	\vfill
	\item<2-> LDA or QDA has smaller training error on the same data?
	\item<3-> What about the test error?
\end{itemize}
\end{frame}


\begin{frame}\frametitle{QDA: Quadratic Discriminant Analysis}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.9}.pdf}\end{center}
\end{frame}

%\begin{frame}\frametitle{Naive Bayes}
%\begin{itemize}
%\item Yet another assumption on class-feature interactions, similar to QDA/LDA
%\begin{center}
%\includegraphics[width=0.5\linewidth]{../figs/class5/naivebayes.jpg}
%\end{center}
%\item With normal distribution over features $X_1, \ldots, X_k$ special case of QDA with \underline{diagonal} $\Sigma$
%\item Generalizes to non-Normal distributions and discrete variables
%\item More on this and other Bayesian models later \ldots
%\end{itemize}
%\end{frame}

\section{Generative Models: Naive Bayes}


\begin{frame}\frametitle{Naive Bayes}
    \textbf{Assumption}: Features are independent given the label
    \begin{gather*}
 \Pr[\varname{income}, \varname{balance} \mid \varname{default} = \text{yes} ] = \\
      \Pr[\varname{income} \mid \varname{default} = \text{yes} ] \cdot
      \Pr[\varname{balance} \mid \varname{default} = \text{yes} ]
    \end{gather*}
    \begin{itemize}
    \item Features may have arbitrary distributions
    \item They are uncorrelated when normal
    \end{itemize}
    General definition (two features)
     \begin{gather*}
 \Pr[X_1 = \tcr{x_1}, X_2=\tcr{x_2} \mid Y = \tcg{y}] = \\ \Pr[X_1 = \tcr{x_1} \mid Y = \tcg{y}] \cdot \Pr[X_2 = \tcr{x_2} \mid Y = \tcg{y}] 
\end{gather*}
\end{frame}


\section{Choosing the Right Model}

\begin{frame} \frametitle{Decision Boundary}
  How to classify with two classes $\tcg{k_1}$ and $\tcr{k_2}$ \\
  \vfill
  \begin{align*}
    \text{Probability in class } \tcg{k_1} &> \text{Probability in class } \tcr{k_2} \\
    \Pr[Y = \tcg{k_1} \mid X = x] &> \Pr[Y = \tcr{k_2} \mid X = x] \\
    \frac{\Pr[Y = \tcg{k_1} \mid X = x]}{\Pr[Y = \tcr{k_2} \mid X = x]} &> 1 \\
    \log \left(  \frac{\Pr[Y = \tcg{k_1} \mid X = x]}{\Pr[Y = \tcr{k_2} \mid X = x]} \right)&> 0 
  \end{align*}
\end{frame}

\begin{frame} \frametitle{Decision Boundaries}
  \begin{align*}
    \text{LR} \quad
    \log \left(  \frac{\Pr[Y = \tcg{k_1} \mid X = x]}{\Pr[Y = \tcr{k_2} \mid X = x]} \right) &= \beta_{0} + \sum_{j = 1}^{p} \beta_{j} \cdot  x_j  \\
    \text{LDA} \quad
    \log \left(  \frac{\Pr[Y = \tcg{k_1} \mid X = x]}{\Pr[Y = \tcr{k_2} \mid X = x]} \right) &= a + \sum_{j = 1}^{p} b_{j} \cdot  x_j \\
    \text{QDA} \quad
   \log \left(  \frac{\Pr[Y = \tcg{k_1} \mid X = x]}{\Pr[Y = \tcr{k_2} \mid X = x]} \right) &= a + \sum_{j = 1}^{p} b_{j} \cdot  x_j + \sum_{j=1}^{p} \sum_{l = 1}^{p} c_{jl} \cdot  x_j x_l \\
                                                                                              \text{NB} \quad
    \log \left(  \frac{\Pr[Y = \tcg{k_1} \mid X = x]}{\Pr[Y = \tcr{k_2} \mid X = x]} \right) &= a + \sum_{j = 1}^{p} g_j (x_j )\\
  \end{align*}
  \end{frame}

  \begin{frame}
    \frametitle{Numerical Comparison 1}
    \begin{center}
     \includegraphics[width=\linewidth]{../islr2figs/Chapter4/4_11.pdf} 
    \end{center}
  \end{frame}

  \begin{frame}
    \frametitle{Numerical Comparison 2}
    \begin{center}
     \includegraphics[width=\linewidth]{../islr2figs/Chapter4/4_12.pdf} 
    \end{center}
  \end{frame}

  \section{Measuring Errors: Confusion Matrix}


\begin{frame} \frametitle{Measuring Error in Classification}
	\begin{itemize}
		\item Mean squared error in regression:
		\[ \varname{MSE} = \frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{f}(x_i))^2  \]
		\item Classification error:
		\[ \varname{CE} = \frac{1}{n} \sum_{i=1}^{n} \mathbf{1}\{y_i \neq \hat{f}(x_i)\}  \]
		\item Accuracy = 1-classification error
	\end{itemize}
\end{frame}


\begin{frame}{Classification Error in Medical Diagnosis}
Two teams tackling medical prediction:
\begin{enumerate}
	\item \textbf{Diabetes}: Relatively common ailment. Achieved prediction accuracy: \alert{$0.955$} = $95.5\% $
	\item \textbf{HIV}: Rare and serious disease. Achieved prediction accuracy: \alert{$0.997$} = $99.7\%$
\end{enumerate}
	Should the bonus go to the HIV team?
\end{frame}

\begin{frame}\frametitle{Understanding Errors: Confusion Matrix}
\begin{center}
	\begin{tabular}{l|l|c|c|c}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{Actual}} & \\
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Yes & \tcg{$a$} & \tcr{$b$} & $a+b$\\
		\cline{2-4}
		& No & \tcr{$c$} & \tcg{$d$} & $c+d$\\
		\cline{2-4}
		\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$a+c$} & \multicolumn{    1}{c}{$b+d$} & \multicolumn{1}{c}{$N$}
	\end{tabular}\\
\pause
\vspace{0.5cm}
	\begin{tabular}{l|l|c|c|}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{Actual}}  \\
		\cline{3-4}
		\multicolumn{2}{c|}{} & Positive & Negative \\
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Positive & \tcg{True Positive} & \tcr{False Positive} \\
		\cline{2-4}
		& Negative & \tcr{False Negative} & \tcg{True Negative} \\
		\cline{2-4}
	\end{tabular}
\end{center}
\end{frame}

\begin{frame}{Confusion Tables in Medical Diagnosis}
\begin{enumerate}
	\item \textbf{Diabetes}: Relatively common ailment. Achieved prediction accuracy: \alert{$0.955$} = $95.5\% $	\\
	\begin{center}
	\begin{tabular}{l|l|c|c|}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\alert{Actual}} \\
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No \\
		\cline{2-4}
		\multirow{2}{*}{\alert{Predicted}}
		& Yes & \tcg{$60$} & \tcr{$10$} \\
		\cline{2-4}
		& No & \tcr{$35$} & \tcg{$895$} \\
		\cline{2-4}
	\end{tabular}
	\end{center}
	\item \textbf{HIV}: Rare and serious disease. Achieved prediction accuracy: \alert{$0.997$} = $99.7\%$ \\
	\begin{center}
	\begin{tabular}{l|l|c|c|}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\alert{Actual}} \\
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No \\
		\cline{2-4}
		\multirow{2}{*}{\alert{Predicted}}
		& Yes & \tcg{$0$} & \tcr{$0$} \\
		\cline{2-4}
		& No & \tcr{$3$} & \tcg{$997$} \\
		\cline{2-4}
	\end{tabular}
	\end{center}
\end{enumerate}

\end{frame}

\begin{frame}\frametitle{Confusion Matrix: Predict $\varname{default}$}
	\begin{center}
	\begin{tabular}{l|l|c|c|c}
	\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{Actual}} & \\
	\cline{3-4}
	\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\
	\cline{2-4}
	\multirow{2}{*}{\textbf{Predicted}}
	& Yes & \tcg{$a$} & \tcr{$b$} & $a+b$\\
	\cline{2-4}
	& No & \tcr{$c$} & \tcg{$d$} & $c+d$\\
	\cline{2-4}
	\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$a+c$} & \multicolumn{    1}{c}{$b+d$} & \multicolumn{1}{c}{$N$}
	\end{tabular}
	\end{center}
\textbf{LDA}: Predict default if $\Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] > \nicefrac{1}{2}$
	\begin{center}
	\begin{tabular}{l|l|c|c|c}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{Actual}} & \\
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Yes & \tcg{$81$} & \tcr{$23$} & $104$\\
		\cline{2-4}
		& No & \tcr{$252$} & \tcg{$9\,644$} & $9\,896$\\
		\cline{2-4}
		\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$333$} & \multicolumn{    1}{c}{$9\,667$} & \multicolumn{1}{c}{$10\,000$}
	\end{tabular}
	\end{center}
\begin{itemize}
	\item<2-> Good predictions?
	\item<3-> \textbf{No}: Most people who default are predicted as No default
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Increasing LDA Sensitivity}
	\textbf{LDA}: Predict default if $\Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] > \nicefrac{1}{2}$
	\begin{center}
	\begin{tabular}{l|l|c|c|c}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{True}} & \\
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Yes & \tcg{$81$} & \tcr{$23$} & $104$\\
		\cline{2-4}
		& No & \tcr{$252$} & \tcg{$9\,644$} & $9\,896$\\
		\cline{2-4}
		\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$333$} & \multicolumn{    1}{c}{$9\,667$} & \multicolumn{1}{c}{$10\,000$}
	\end{tabular}
	\end{center}
\visible<2->{
	\textbf{Result of LDA classification}: Predict default if $\Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] > \nicefrac{1}{5}$
	\begin{center}
	\begin{tabular}{l|l|c|c|c}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{Actual}} & \\
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Yes & \tcg{$195$} & \tcr{$235$} & $403$\\
		\cline{2-4}
		& No & \tcr{$138$} & \tcg{$9\,432$} & $9\,570$\\
		\cline{2-4}
		\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$333$} & \multicolumn{    1}{c}{$9\,667$} & \multicolumn{1}{c}{$10\,000$}
	\end{tabular}
	\end{center}
}
\end{frame}

\begin{frame}\frametitle{True Positives, etc}
	\begin{center}
	\begin{tabular}{l|l|c|c|}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{Actual}}  \\
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No \\
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Positive & \tcg{True Positive (\textbf{TP})} & \tcr{False Positive (\textbf{FP})} \\
		\cline{2-4}
		& Negative & \tcr{False Negative (\textbf{FN})} & \tcg{True Negative (\textbf{TN})} \\
		\cline{2-4}
	\end{tabular}\end{center}
	\vspace{3mm}
	\begin{itemize}
		\item \textbf{TPR/recall/sensitivity} = TP/(TP+FN), $\mathbb{P}$ of positive for yes
		\item \textbf{TNR/specificity} = TN/(FP+TN), $\mathbb{P}$ of negative for no
		\item \textbf{FPR} = FP/(FP+TN), $\mathbb{P}$ of positive for no
		\item \textbf{Precision} = TP/(TP+FP), $\mathbb{P}$ of yes for positive
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{ROC Curve}
	\begin{center}\includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter4/4.8}.pdf}\end{center}

	\begin{itemize}
		\item \textbf{True positive rate} = TP/(TP+FN), $\mathbb{P}$ of positive for yes
		\item \textbf{False positive rate} = FP/(FP+TN), $\mathbb{P}$ of positive for no
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Area Under ROC Curve}
	\begin{center}\includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter4/4.8}.pdf}\end{center}
	\begin{itemize}
		\item A larger area is better
		\item Many other ways to measure classifier performance, like $F_1$, TSS
	\end{itemize}
\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
