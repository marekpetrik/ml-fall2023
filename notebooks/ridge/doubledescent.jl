using StatsPlots
using LinearAlgebra
using CSV
using DataFrames
using Random: shuffle
using ClassicalOrthogonalPolynomials
using ProgressMeter
pgfplotsx()

autos = CSV.read("Auto.csv", DataFrame)
autos = autos[1:30,:]

@df autos scatter(:weight, :acceleration, xlabel="weight", ylabel="acceleration")
scale = maximum(autos.weight)

function ridge(X,y,λ)
    (X' * X + λ * I)  \ X' * y
end


function generate_errors()

    degree = 1:200
    test_errs = zeros(length(degree))
    train_errs = zeros(length(degree))
    nruns = 300

    @showprogress for iter ∈ 1:nruns
        
        neworder = shuffle(1:nrow(autos))
        train = autos[neworder[begin:(end ÷ 2)],:]
        test = autos[neworder[(end ÷ 2 + 1):end],:] 

        ytrain::Vector{Float64} = train.acceleration
        ytest::Vector{Float64} = test.acceleration

        xtrain::Vector{Float64} = train.weight / scale
        xtest::Vector{Float64} = test.weight / scale


        for (k,d) ∈ enumerate(degree)
            # *****
            # raw polynomials do not work
            # numerical issues
            #Xtrain = [xtrain[i]^j for i ∈ 1:nrow(train), j ∈ 0:d ]
            #Xtest = [xtest[i]^j for i ∈ 1:nrow(test), j ∈ 0:d ]

            # ***
            # use orthogonal polynomials instead
            Xtrain = [legendrep(j, xtrain[i]) for i ∈ 1:nrow(train), j ∈ 0:d ]
            Xtest = [legendrep(j, xtest[i]) for i ∈ 1:nrow(test), j ∈ 0:d ]

            β = ridge(Xtrain, ytrain, 1e-8)

            err_train = norm(Xtrain * β - ytrain) / √nrow(train)
            err_test = norm(Xtest * β - ytest) / √nrow(test)

            train_errs[k] += err_train / nruns
            test_errs[k] += err_test / nruns

            #println(err_train, "    ", err_test)

            #ord = sortperm(view(Xtrain, :, 2))

            #scatter(train.weight/scale, train.acceleration,
            #        xlabel="weight", ylabel="acceleration")
            #plot!(view(Xtrain, ord, 2), view(Xtrain * β, ord)) |> display
        end
    end
                       
    (degree, train_errs, test_errs)
end

(degree, trainerr, testerr) = generate_errors()

plot(trainerr, label = :none, xlabel = "Polynomial degree", ylabel = "Training RMSE",
     size = (300,250))
savefig("doubledescent-train.pdf")
plot(testerr, label = :none, xlabel = "Polynomial degree", ylabel = "Test RMSE",
     size = (300,250))
savefig("doubledescent-test.pdf")
