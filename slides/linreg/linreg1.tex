\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{bigdelim,multirow}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

\renewcommand{\Pr}{\mathbb{P}}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\definecolor{maroon(x11)}{rgb}{0.69, 0.19, 0.38}
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\tcm}[1]{\tc{maroon(x11)}{#1}}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\Real}{\mathbb{R}}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\I}{\mathbb{I}}
\newcommand{\sd}{\operatorname{sd}}
\newcommand{\cov}{\operatorname{Cov}}
\newcommand{\corr}{\operatorname{corr}}
\let\Var\undefined
\DeclareMathOperator{\RSS}{RSS}
\DeclareMathOperator{\Var}{Var}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

%\newcommand{\Pr}{\mathbb{P}}

\AtBeginSection[]{
	\begin{frame}
          \vfill
	\centering
	% \usebeamerfont{title}
        {\huge\bf \insertsectionhead}%
	\vfill
\end{frame}
}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Linear Regression}
\subtitle{ISLR 2}
\author{Marek Petrik}
\date{9/7/2023}

\begin{document}



\begin{frame}
	\maketitle
	\tiny{Some of the figures in this presentation are taken from "An Introduction to Statistical Learning, with applications in R"  (Springer, 2013) with permission from the authors: G. James, D. Witten,  T. Hastie and R. Tibshirani }
\end{frame}


\begin{frame}\frametitle{Linear Regression}
\begin{itemize}
\item \textbf{Today:} What is linear regression
\begin{enumerate}
\item Correlation and samples
\item What is linear regression
\item Evaluating fit
\item Multiple features
\item Nonlinear linear regression
\end{enumerate}
\pause
\vfill
\item \textbf{Next time:} How to really make it work
\begin{itemize}
\item How things may go wrong
\item How to fix and diagnose it
\item Solving linear regression
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Machine Learning Choices ...}
    \centering
    \includegraphics[width=\linewidth]{../figs/class1/ml_map.png}\\
    {\tiny Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/index.html}} \\[3mm]
\end{frame}

\begin{frame}
  \frametitle{Covariance and Correlation}
  Covariance
\[ \cov(X,Y) = \E[(X - \E[X]) \cdot (Y - \E[Y]) ] \]  
Between $(-\infty , \infty )$
\vfill 
Correlation
\[ \corr(X,Y) = \frac{\cov(X,Y)}{\sd[X]\cdot \sd[Y]} \]
Between $[-1, +1]$
    \begin{description}
        \item[$0$:] Variables are not related
        \item[$1$:] Variables are perfectly  related (same)
        \item[$-1$:] Variables are negatively related (different)
    \end{description}
\end{frame}

\begin{frame} \frametitle{Sample Statistics}
    Sample: $x_1, x_2, \ldots, x_n$ \\
    \vfill
    \textbf{Sample Mean}:
    \[ \bar{X} = \frac{1}{n} \sum_{i=1}^n x_i \]
    \textbf{Sample Variance (Biased!)}:
    \[ \sigma^2_X = \frac{1}{n} \sum_{i=1}^n (x_i - \bar{X})^2  \]
    \textbf{Sample Variance (Unbiased)}:
    \[ \sigma^2_X = \frac{1}{n-1} \sum_{i=1}^n (x_i - \bar{X})^2  \]
    Elementary proof: {\tiny\url{https://en.wikipedia.org/wiki/Variance\#Sample_variance}}
\end{frame}

\section{Introducing Linear Regression}

\begin{frame}{Why Linear Regression: Simple and Efficient}
\centering
\begin{minipage}{0.4\linewidth}
	\includegraphics[width=\linewidth]{../figs/class2/powder_mill_bridge.pdf}
	{\small Powder Mill Bridge, MA}
\end{minipage}
\hspace{0.15\linewidth}
\begin{minipage}{0.4\linewidth}
	\includegraphics[width=\linewidth]{../figs/class2/powder_mill_bridge_sens.pdf}
	{\small Strain gauges, PMB, MA}
\end{minipage}\\
\vspace{0.6cm}
\begin{minipage}{0.9\linewidth}
	{\small Bridge damage detection success (higher is better)} \\
	\includegraphics[width=\linewidth]{../figs/class2/kathryn_results.pdf}
	{\small Training time: LR: seconds, ANN: days} {\tiny[Kathryn Kaspar, A protocol for using long-term structural health monitoring data to detect and localize damage in bridges, UNH MS Thesis, 2018]}
\end{minipage}
\end{frame}

\begin{frame} \frametitle{What is Machine Learning}
\begin{itemize}
\item Discover unknown function $\alert{f}$:
\[ \tcb{Y} = \tcr{f}(\tcg{X}) + \epsilon \]
\item $\tcr{f}$ = \textbf{hypothesis}, or model
\item $\tcg{X}$ = \textbf{features}, or predictors, or inputs
\item $\tcb{Y}$ = \textbf{response}, or target
\end{itemize}
\end{frame}

\begin{frame}\frametitle{How Good are Predictions?}
    \begin{itemize}
        \item Learned function $\hat{f}$
        \item Test data: ${(x_1,y_1), (x_2, y_2), \ldots}$
        \item \textbf{Mean Squared Error (MSE)}:
        \[
        \varname{MSE} = \frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{f}(x_i))^2
        \]
%        \item This is the estimate of:
%        \[ \varname{MSE} = \Ex[(Y - \hat{f}(X))^2]  = \frac{1}{|\Omega|} \sum_{\omega\in\Omega} (Y(\omega) - \hat{f}(X(\omega)))^2 \]
        \item \textbf{Root Mean Squared Error (RMSE)}: Unchanged units
        \[
        \varname{RMSE} = \sqrt{\varname{MSE}}
        \]
        \item Important: Errors in labels $y_i$ are i.i.d.
    \end{itemize}
  \end{frame}

\section{Training and Test Data}

\begin{frame} \frametitle{Prediction Error: Training and test data sets}
    \centering
    \begin{tabular}{rrrll}
        \hline
        & \tcb{mpg} & \tcg{horsepower} & name & \\
        \hline
        1 & 18.00 & 130.00 & chevrolet chevelle malibu & \rdelim\}{3}{3mm}[test] \\
        2 & 15.00 & 165.00 & buick skylark 320 &\\
        3 & 18.00 & 150.00 & plymouth satellite &\\
        \hline \hline
        4 & 16.00 & 150.00 & amc rebel sst & \rdelim\}{7}{3mm}[training] \\
        5 & 17.00 & 140.00 & ford torino & \\
        6 & 15.00 & 198.00 & ford galaxie 500 & \\
        7 & 14.00 & 220.00 & chevrolet impala & \\
        8 & 14.00 & 215.00 & plymouth fury iii & \\
        9 & 14.00 & 225.00 & pontiac catalina & \\
        10 & 15.00 & 190.00 & amc ambassador dpl & \\
        \hline
    \end{tabular}
\end{frame}


\begin{frame} \frametitle{Do We Need Test Data?}
    Why not just test on the training data?
    \pause
    \begin{center}            \includegraphics[width=0.8\linewidth]{../figs/class1/auto_knn_compare.pdf}
    \end{center}
\end{frame}

\begin{frame} \frametitle{KNN Error}
    \includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter2/2.17}.pdf}
\end{frame}
\begin{frame} \frametitle{Simple Linear Regression}
	\begin{itemize}
          \item The function $f$ is \emph{linear} and only one feature:
			\[ \tcr{Y} \approx \tcg{\beta_0} + \tcb{\beta_1} X \qquad \tcr{Y} = \tcg{\beta_0} + \tcb{\beta_1} X  + \epsilon \]
		\item Example:
			\begin{center}\includegraphics[width=0.75\linewidth]{{../figs/class2/sales_tv_reg}.pdf}\end{center}
    \[ \tcr{\textrm{Sales}} \approx \tcg{\beta_0} + \tcb{\beta_1} \times \varname{TV} \]
    \end{itemize}
\end{frame}

\section{Fitting Linear Regression}
\begin{frame} \frametitle{How To Estimate Coefficients}
\begin{itemize}
\item No line that will have no errors on data $x_i$
\item Prediction:
\[ \tcr{\hat{y}_i} = \tcg{\hat\beta_0} + \tcb{\hat{\beta_1}} x_i\]
\item Errors ($y_i$ are true values):
\[ e_i = \tcm{y_i} - \tcr{\hat{y}_i}  \]
\begin{center}
    \includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter3/3.1}.pdf}
\end{center}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Residual Sum of Squares}
\begin{itemize}
    \item Residual Sum of Squares
    \[ \RSS = e_1^2 + e_2^2 + e_3^2 + \cdots + e_n^2  = \sum_{i=1}^n e_i^2\]
    \item Equivalently:
    \[ \RSS = \sum_{i=1}^n ( \tcm{y_i} - \tcg{\hat{\beta}_0} - \tcb{\hat\beta_1} x_i )^2 \]
\end{itemize}
\end{frame}


\begin{frame} \frametitle{Notation}
  \begin{itemize}
  \item $i$ = index of the observation
  \item $x_i$ = feature of $i$-th observation
  \item $\beta_0 + \beta_1 x_i$ = prediction for $i$-th observation
  \item $Y_i$ = random target value; represents possible values
  \item $\epsilon_i = Y_i - \beta_0 + \beta_1 x_i$ = random noise of $i$-th prediction
  \item $y_i$ = observed value of $Y_i$
  \item $e_i = y_i - \beta_0 + \beta_1 x_i$ = instantiation of noise $\epsilon_i$
  \item $C$ = some constant (not always the same) independent of $\beta$
  \end{itemize}
\end{frame}


\begin{frame} \frametitle{Why Minimize RSS}
    \begin{enumerate}
        \pause
        \item It is convenient: can be solved in closed form
        \vfill
        \pause
        \item Maximize likelihood when $Y_i = \beta_0 + \beta_1 x_i + \epsilon_i$ when $\epsilon_i \sim \mathcal{N}(0,1)$
        \vfill
        \pause
        \item Best Linear Unbiased Estimator (BLUE): Gauss-Markov Theorem (ESL 3.2.2)
    \end{enumerate}
\end{frame}


\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
    \begin{itemize}
            \item \textbf{Likelihood}: Probability that data is generated from a model \only<2>{(\alert{i.i.d. assumption})}
            \only<1>{\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
            \only<2>{\[ \ell(\tcr{\beta_0,\beta_1}) = \Pr[\tcb{Y_1=y_1,Y_2=y_2,\ldots} \mid \tcr{\beta_0,\beta_1}] = \prod_{i=1}^n \operatorname{pdf}(\tcb{y_i} \mid \tcr{\beta_0,\beta_1}) \]}
            \item Find the most likely model:
            \only<1>{\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
            \only<2>{\[ \max_{\tcr{\beta_0,\beta_1}} \ell(\tcr{\beta_0,\beta_1}) = \max_{\tcr{\beta_0,\beta_1}} \Pr[\tcb{Y_1=y_1,Y_2=y_2,\ldots} \mid \tcr{\beta_0,\beta_1}]  \]}
            \item Likelihood function is difficult to maximize
            \item Transform it using $\log$ (strictly increasing)
            \only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]}
            \only<2>{\[ \max_{\tcr{\beta_0,\beta_1}} \log \ell(\tcr{\beta_0,\beta_1}) = \max_{\tcr{\beta_0,\beta_1}} \sum_{i=1}^n \log \Pr[\tcb{Y_i = y_i} \mid \tcr{\beta_0,\beta_1}] \]}
            \item Strictly increasing transformation preserves maximizer
    \end{itemize}
\end{frame}

\begin{frame} \frametitle{Max-likelihood: Linear Regression}
    \begin{itemize}
            \item \textbf{Parameters}:
            \[ f_{\tcr{\beta}}(x) = \tcr{\beta_0}+\tcr{\beta_1} \, x \]
            \item \textbf{Data}: Features $\tcb{x_i}$ and labels $\tcb{y_i}$
            \item \textbf{Assumption}: $Y_i \sim \mathcal{N}(f_{\tcr{\beta}}(\tcb{x_i}),1)$ or $Y_i = f_{\tcr{\beta}}(\tcb{x_i}) + \epsilon_i$, $\epsilon_i \sim \mathcal{N}(0,1)$, i.i.d.
            \item Likelihood:  Use PDF $\operatorname{pdf}$ because $\epsilon_i$ is continuous! 
              \[
                \ell(\tcr{\beta_0},\tcr{\beta_1})
                = \prod_{i=1}^{n} \operatorname{pdf}( \tcb{y_{i}} \mid \mu = f_{\tcr{\beta}}(\tcb{x_i}), \sigma = 1) 
                = \prod_{i=1}^{n} \operatorname{pdf}( e_i)
              \]
            \item Log-likelihood ($C > 0$ constant independent of $\tcr{\beta}_0, \tcr{\beta}_1$):
            \[ \log \ell(\tcr{\beta_0},\tcr{\beta_1}) = - \frac{1}{2} \sum_{i=1}^{n}  ( \tcb{y_i} - \tcr{\beta_0} - \tcr{\beta_1}\cdot \tcb{x_i})^{2} + C \]
            \item Concave maximization problem (convex minimization problem)
    \end{itemize}
\end{frame}

\section{Evaluating Fit}

\begin{frame}\frametitle{How Good is Fit?}
    \[ \varname{Sales} = f(\varname{TV}, \varname{Radio}, \varname{Newspaper}) \]
    \begin{center}\includegraphics[width=0.75\linewidth]{{../islrfigs/Chapter2/2.1}.pdf}\end{center}
    \vfill
    \begin{itemize}
        \item How well is linear regression predicting the training data?
        \item Can we be sure that TV advertising really influences the sales?
        \item \alert{Does RSS/MSE answer these questions?}
        \item \alert{Does log likelihood work?}
    \end{itemize}
\end{frame}

\begin{frame}\frametitle{$R^2$ Statistic}
\[ R^2 = 1 - \frac{\operatorname{RSS}}{\operatorname{TSS}} = 1 - \frac{\sum_{i=1}^n (\tcm{y_i} - \tcr{\hat{y}_i})^2 }{\sum_{i=1}^n (\tcm{y_i} - \tcb{\bar{y}})^2} \]
\begin{itemize}
\item RSS - residual sum of squares, TSS - total sum of squares
\item $R^2$ measures the goodness of the fit as a proportion
\item Proportion of data variance explained by the model
\item Extreme values:
\begin{description}
	\item[$0$:] Model does not explain data
	\item[$1$:] Model explains data perfectly
\end{description}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Example: TV Impact on Sales}
\begin{center}
\includegraphics[width=0.9\linewidth]{{../figs/class2/sales_tv_reg}.pdf} \\
\visible<2>{$R^2 = 0.61$}
\end{center}
\end{frame}

\begin{frame} \frametitle{Example: Radio Impact on Sales}
\begin{center}
\includegraphics[width=0.9\linewidth]{{../figs/class2/sales_radio}.pdf} \\
\visible<2>{$R^2 = 0.33$}
\end{center}
\end{frame}

\begin{frame} \frametitle{Example: Newspaper Impact on Sales}
	\begin{center}
	\includegraphics[width=0.9\linewidth]{{../figs/class2/sales_newspaper}.pdf} \\
	\visible<2>{$R^2 = 0.05$}
	\end{center}
\end{frame}

\begin{frame} \frametitle{$R^2$ Statistic and Correlation Coefficient}
    \visible<2->{\[ R^2 = \corr(X,Y)^2 \]}
    \vfill
    \visible<3->{
        Correlation coefficient $\corr(X,Y)$:
        \begin{description}
            \item[$0$:] Variables are not related, $R^2 = 0$
            \item[$1$:] Variables are perfectly  related (same), $R^2 = 1$
            \item[$-1$:] Variables are negatively related (different), $R^2 = 1$
    \end{description}}
\end{frame}

\section{Multiple Linear Regression}

\begin{frame} \frametitle{Multiple Linear Regression}
    \begin{itemize}
        \item Usually more than one feature is available
        \[ \varname{sales} = \beta_0 + \beta_1 \times \varname{TV} + \beta_2 \times \varname{radio} + \beta_3 \times \varname{newspaper}  + \epsilon \]
        \item In general:
        \[ Y = \beta_0 + \sum_{j=1}^p \beta_j X_j \]
    \end{itemize}
\end{frame}


\begin{frame} \frametitle{Multiple Linear Regression}
    \begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.4}.pdf}\end{center}
\end{frame}


\section{Feature Engineering}


\begin{frame} \frametitle{Feature Engineering}
	\begin{center}
		What if we have \ldots
	\end{center}
	\begin{enumerate}
		\item Qualitative features: (gender, car color, major)
		\item Interaction between features: non-additivity
		\item Nonlinear relationships
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Qualitative Features: 2 Values}
	\begin{itemize}
		\item Predict $\varname{salary}$ as a function of $\varname{gender}$
		\item Feature $\varname{gender}_i \in \{ \operatorname{male}, \operatorname{female} \}$
		\item<2-> Introduce \textbf{indicator variable} $x_i$: (AKA dummy variable, \ldots)
		\[x_i = \begin{cases}
			0 & \text{if } \varname{gender}_i = \operatorname{male} \\
			1 & \text{if } \varname{gender}_i = \operatorname{female} \\
		\end{cases} \]
		\item<2-> Predict salary as:
		\[ \varname{salary} = \beta_0 + \beta_1 \times x_i  = \begin{cases}
			\beta_0 & \text{if } \varname{gender}_i = \operatorname{male} \\
			\beta_0 + \beta_1 & \text{if } \varname{gender}_i = \operatorname{female} \\
		\end{cases} \]
		\item<3-> $\beta_1$ is the difference between female and male salaries
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Qualitative Features: Many Values}
\begin{itemize}
\item Predict $\varname{salary}$ as a function of $\varname{state}$
\item Feature $\varname{state}_i \in \{ \operatorname{MA}, \operatorname{NH}, \operatorname{ME} \}$
\item What about $x_i$:
\[x_i = \begin{cases}
0 & \text{if } \varname{state}_i = \operatorname{MA} \\
1 & \text{if } \varname{state}_i = \operatorname{NH} \\
2 & \text{if } \varname{state}_i = \operatorname{ME} \\
\end{cases} \]
\item<2-> Predict salary as:
\[ \varname{salary} = \beta_0 + \beta_1 \times x_i  = \begin{cases}
\beta_0  & \text{if } \varname{state}_i = \operatorname{MA} \\
\beta_0 + \beta_1 & \text{if } \varname{state}_i = \operatorname{NH} \\
\beta_0 + 2\times \beta_1 & \text{if } \varname{state}_i = \operatorname{ME} \\
\end{cases} \]
\item<3> \alert{Does not work}: NH salary always average of MA and ME
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Qualitative Features: Many Values The Right Way}
\begin{itemize}
	\item Predict $\varname{salary}$ as a function of $\varname{state}$
	\item Feature $\varname{state}_i \in \{ \operatorname{MA}, \operatorname{NH}, \operatorname{ME} \}$
	\item<2-> Introduce 2 \textbf{indicator variables} $x_i, z_i$:
	\[x_i = \begin{cases}
	0 & \text{if } \varname{state}_i \neq \operatorname{MA} \\
	1 & \text{if } \varname{state}_i = \operatorname{MA} \\
	\end{cases} \qquad
	z_i = \begin{cases}
	0 & \text{if } \varname{state}_i \neq \operatorname{NH} \\
	1 & \text{if } \varname{state}_i = \operatorname{NH} \\
	\end{cases}\]
	\item<2-> Predict salary as:
	\[ \varname{salary} = \beta_0 + \beta_1 \times x_i + \beta_2 \times z_i  =
	\begin{cases}
	\beta_0 + \beta_1 & \text{if } \varname{state}_i = \operatorname{MA} \\
	\beta_0 + \beta_2 & \text{if } \varname{state}_i = \operatorname{NH} \\
	\beta_0 		  & \text{if } \varname{state}_i = \operatorname{ME} \\
	\end{cases} \]
	\item<3-> \alert{Need an indicator variable for ME? Why?} hint: linear independence
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Removing Additive Assumption}
	\begin{itemize}
		\item What is the additive assumption?
		\[ \varname{sales} = \beta_0 + \beta_1 \times \varname{TV} + \beta_2 \times \varname{radio}  \]
		\item What if $\varname{TV}$ and $\varname{radio}$ interact?
		\item<2-> Add new feature:
		\[ \varname{sales} = \beta_0 + \beta_1 \times \varname{TV} + \beta_2 \times \varname{radio} + \beta_3 \times \varname{TV} \times \varname{radio} \]
	\end{itemize}
\end{frame}

\begin{frame}	\frametitle{Example of Interaction}
	\vspace{-10mm}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.7}.pdf}\end{center}%
	\vspace{-10mm}
	\begin{columns}[T]
	\begin{column}{0.5\linewidth}
		\begin{gather*}
		\varname{balance}_i = \\
		\beta_0 + \\
		\beta_1\times\varname{income}_i  + \\
		\beta_2 \times\varname{student}_i
		\end{gather*}
	\end{column}
	\begin{column}{0.5\linewidth}
		\begin{gather*}
		\varname{balance}_i = \\
		\beta_0 + \beta_1\times\varname{income}_i + \\
		\beta_2 \times\varname{student}_i + \\
		\beta_3 \times\varname{student}_i \times \varname{income}_i
		\end{gather*}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame} \frametitle{Nonlinear Relationship}
	Can we use linear regression to fit a nonlinear function?
	\vspace{-6mm}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.8}.pdf}\end{center}%
	\vspace{-10mm}
\end{frame}

\begin{frame} \frametitle{Nonlinear Relationship}
	\begin{itemize}
		\item Linear regression can fit a nonlinear function
		\item Just introduce new features!
		\item Linear regression:
		\[ \varname{mpg} = \beta_0 + \beta_1 \times \varname{power} \ \]
		\item Degree $2$ (Quadratic):
		\[ \varname{mpg} = \beta_0 + \beta_1 \times \varname{power} + \beta_2 \times \varname{power}^2 \]
		\item Degree $k$:
		\[ \varname{mpg} = \sum_{i=0}^{k} \beta_k \times \varname{power}^k \]
	\end{itemize}
\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
