\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}
\newcommand{\Real}{\mathbb{R}}

\title{Gradient Descent}

\author{Marek Petrik}
\date{October 3, 2023}


\AtBeginSection[]{
	\begin{frame}
          \vfill
	\centering
	% \usebeamerfont{title}
        {\huge\bf \insertsectionhead}%
	\vfill
\end{frame}
}

\begin{document}

\begin{frame} \maketitle \end{frame}

\begin{frame} \frametitle{Logistic Regression: Likelihood Function}
    \begin{itemize}
            \item \textbf{Data}: Features $\tcb{x_i}$ and labels $\tcb{y_i}$
            \item \textbf{Parameters}:
            \[ p_{\tcr{\beta}}(x) = \frac{e^{\tcr{\beta_0}+\tcr{\beta_1} \, x}}{1+ e^{\tcr{\beta_0} + \tcr{\beta_1}\,x}}  \]
            \item \textbf{Assumption}: $\Pr[Y_i = 1] = p_{\tcr{\beta}}(\tcb{x_i})$, i.i.d.
            \item Likelihood:
            \[ \ell(\tcr{\beta_0},\tcr{\beta_1})  = \prod_{i : \tcb{y_i} =1} p_{\tcr{\beta}}(\tcb{x_i}) \prod_{i:\tcb{y_i}=0} (1-p_{\tcr{\beta}}(\tcb{x_i})) \]
            \item Log-likelihood:
            \[ \log \ell(\tcr{\beta_0},\tcr{\beta_1}) = \sum_{i: \tcb{y_i} =1} \log p_{\tcr{\beta}}(\tcb{x_i}) + \sum_{i:\tcb{y_i}=0} \log (1-p_{\tcr{\beta}}(\tcb{x_i})) \]
            \item Concave maximization problem
            \item No closed-form solution
    \end{itemize}
    % julia: plot function
    % using Plots
    % ll(β) =  3*log(exp(β)/(1+exp(β)))+ 5*log(1-exp(β)/(1+exp(β)))
    % plot(ll, xlim=(-0.1, 0.1))
\end{frame}

\section{Taylor Series}


\begin{frame} \frametitle{First Order Expansion: Single Dimension}
    If $f\colon \Real \to \Real $ is twice differentiable, then
    \[
        f(x) = f(x_0) + f'(x_0) (x - x_0) + o\left(x - x_0\right)
      \]
      Here, $o(y)$ is some function $g(y)$ such that
      \[
       \lim_{y\to 0}  \frac{g(y)}{y} = 0
     \]
     \pause
     Intuition: Looking closely enough, function $f$ is linear
      
    % julia: plot function  
    % using Plots
    % ll(β) =  3*log(exp(β)/(1+exp(β)))+ 5*log(1-exp(β)/(1+exp(β)))
    % dll(β) = 3 - 3*exp(β)/(1+exp(β)) - 5*exp(β)/(1+exp(β))
    % tla(β, β₀) = ll(β₀) + (β - β₀) * dll(β₀)
    % plot(ll, xlim=(-0.1, 0.1))
    % plot!(β->tla(β, 0.5))
   \end{frame}


\begin{frame} \frametitle{First Order Expansion: Vector Space}
    If $f\colon \Real^n \to \Real$ is twice differentiable, then
    \[
        f(x) = f(x_0) + \nabla f(x_0)^T (x - x_0) + o\left(\|x - x_0 \|\right)
    \]
    Here, $o(y)$ is some function $g(y)$ such that
    \[
        \lim_{y\to 0}  \frac{g(y)}{y} = 0
    \]
    \pause
    Intuition: Looking closely enough, a function $f$ is linear
\end{frame}

\section{Gradient Descent and Ascent}

\begin{frame} \frametitle{Gradient Methods}
  \textbf{Gradient ascent}: Maximizing a function
  \[
   \max_{x} f(x) 
  \]

 \textbf{Gradient descent}: Minimizing a function
  \[
   \min_{x} f(x) 
 \]
\end{frame}


\begin{frame} \frametitle{Gradient Descent: Main Idea}
  \begin{enumerate}
  \item Start with initial point $x_0$
  \item Assume that $f$ is \emph{linear} around $x_0$
    \[
     f(x) \approx  f(x_0) + \nabla f(x_0)^T (x - x_0) 
    \]
  \item Compute a direction along the gradient of the linear function to minimize it
  \item Take a step along the gradient. Step must not be too long, because the function is not linear
  \item Compute $x_1$ and repeat until in stationary point
  \end{enumerate}
\end{frame}

\begin{frame}\frametitle{Gradient Descent}
    \begin{enumerate}
        \item Start with some initial point $x_0$
        \item Initialize counter: $i \gets 0$
        \item Compute direction: $\Delta x \gets -\nabla f(x_i)$
        \item Compute step size: $t$ (diminishing with iteration)
        \item Update solution: $x_{i+1} \gets x_i + t \cdot \Delta x$
        \item Let $i \gets i + 1$ and \underline{goto} 3
    \end{enumerate}
\end{frame}

\begin{frame}{Gradient Descent: When to use it}

Objective function $f(x)$ is:

\begin{enumerate}
	\item Differentiable (continuous)
	\item Strongly convex
\end{enumerate}

\end{frame}

\begin{frame}{Log-Likelihoods}
\begin{itemize}
	\item Linear regression (Gaussian noise with $\sigma^2 = 1$)
	\[ \ell(\beta) = -\frac{1}{2} \sum_{i=1}^n \left(  y_i - \hat{y}(x_i; \beta)\right)^2 + O(1)  \]
	\item Logistic regression, $p(x_i;\beta) =$ prediction prob. (see ESL 4.4.1)
	\begin{align*}
		\ell(\beta) &= \sum_{i=1}^{n} \Bigl( y_i \log p(x_i; \beta) + (1-y_i) \log (1 - p(x_i; \beta)) \Bigr) = \\
		&= \sum_{i=1}^{n} \left( y_i \sum_{j=1}^k \beta^j x_i^j + (1-y_i) \log \left(1 + e^{\sum_{j=1}^k \beta^j x_i^j} \right) \right)
	\end{align*}
\end{itemize}
\end{frame}

%\begin{frame}
%\centering
%\includegraphics[width=\linewidth]{../figs/class6_5/gunstock.jpg}
%\end{frame}

\begin{frame}{Second Order Methods}
\begin{itemize}
	\item \textbf{First-order methods}: Gradient descent, SGD, Nesterov's acceleration, \ldots
	\[ f(x) = f(x_0) + \nabla f(x_0) (x-x_0) + o(\| x - x_0 \|)\]
	\item \textbf{Second-order methods}: Newton's method, Quasi-Newton, BFGS, \ldots
          \[
            \begin{gathered}
              f(x) = f(x_0) + \nabla f(a) (x-x_0) +  \\
              + \frac{1}{2} (x-x_0)^T \underbrace{\nabla^2 f(x_0)}_{\text{Hessian}} (x-x_0) + o(\| x - x_0\|^2)
            \end{gathered}
            \]
\end{itemize}
\end{frame}

\begin{frame}{Computational Tools}
 See R notebook
% \begin{center}
% \includegraphics[width=\linewidth]{../figs/class6_5/cannon.jpg}
% \end{center}
\end{frame}

\end{document}